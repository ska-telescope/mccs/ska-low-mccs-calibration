"""Configuration file for Sphinx."""

import os
import sys

sys.path.insert(0, os.path.abspath("../../src"))

project = "ska-low-mccs-calibration"
copyright = "2024, SKAO"
author = "See CONTRIBUTORS"
release = "1.0.1"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_autodoc_typehints",
]

exclude_patterns = ["links.rst"]

# Set up common links
rst_epilog = ""
with open("links.rst") as f:
    rst_epilog += f.read()

html_css_files = [
    "css/custom.css",
]

html_theme = "ska_ser_sphinx_theme"
html_theme_options = {}

autodoc_mock_imports = [
    "pygdsm",  # does some large downloads at package install.
]

intersphinx_mapping = {
    "astropy": ("https://docs.astropy.org/en/v5.3.4/", None),
    "numpy": ("https://numpy.org/doc/1.23/", None),
    "python": ("https://docs.python.org/3.10", None),
    # Actually using xarray 2023.12.0 but those docs are broken
    "xarray": ("https://docs.xarray.dev/en/v2023.11.0/", None),
}

nitpicky = True

nitpick_ignore = [
    # intersphinx is just generally awful at cross-referencing numpy
    # https://github.com/sphinx-doc/sphinx/issues/10974#issuecomment-1315385037
    ("py:class", "collections.abc.Buffer"),
    ("py:class", "matplotlib.figure.Figure"),
    ("py:class", "numpy.complex64"),
    ("py:class", "numpy.complex128"),
    ("py:class", "numpy.float_"),
    ("py:class", "numpy.float32"),
    ("py:class", "numpy.float64"),
    ("py:class", "numpy.int64"),
    ("py:class", "numpy._typing._array_like._SupportsArray"),
    ("py:class", "numpy._typing._dtype_like._DTypeDict"),
    ("py:class", "numpy._typing._dtype_like._SupportsDType"),
    ("py:class", "numpy._typing._nested_sequence._NestedSequence"),
    ("py:class", "numpy.typing.ArrayLike"),
    ("py:class", "numpy.typing.DTypeLike"),
    ("py:class", "numpy.typing.NDArray.numpy.bool_"),
    ("py:class", "numpy.typing.NDArray.numpy.int_"),
    ("py:class", "numpy.typing.NDArray.typing.Union[numpy.float32, numpy.float64]"),
    (
        "py:class",
        "numpy.typing.NDArray.typing.Union[numpy.complex64, numpy.complex128]",
    ),
    ("py:class", "numpy.typing.NDArray.numpy.int_"),
]
