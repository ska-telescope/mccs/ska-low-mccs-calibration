.. _figure: https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.figure.html
.. _h5py: https://docs.h5py.org/en/stable
.. _healpy: https://healpy.readthedocs.io/en/latest
.. _PyGDSM: https://github.com/telegraphic/pygdsm

.. _SDP Data Models: https://developer.skao.int/projects/ska-sdp-datamodels/en/latest
.. _GainTable: https://developer.skao.int/projects/ska-sdp-datamodels/en/latest/api/ska_sdp_datamodels.calibration.GainTable.html
.. _Visibility: https://developer.skao.int/projects/ska-sdp-datamodels/en/latest/api/ska_sdp_datamodels.visibility.Visibility.html

.. _SDP Func-Python: https://developer.skao.int/projects/ska-sdp-func-python/en/latest
.. _apply_gaintable: https://developer.skao.int/projects/ska-sdp-func-python/en/latest/api/ska_sdp_func_python.calibration.operations.apply_gaintable.html
.. _solve_gaintable: https://developer.skao.int/projects/ska-sdp-func-python/en/latest/api/ska_sdp_func_python.calibration.solvers.solve_gaintable.html
