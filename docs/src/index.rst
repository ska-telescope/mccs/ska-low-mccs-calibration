SKA-Low MCCS Calibration
========================

This module contains the functions necessary to take observed visibilities
between the log-periodic antennas of a SKA-Low station, create a sky model and
return antenna-based calibration solutions. It will support the creation and
incremental updating of a calibration database. The module makes heavy use of
SDP `data models <SDP Data Models_>`_ and
`processing functions <SDP Func-Python_>`_.

Installation Instructions
=========================

The package is installable via pip::

    pip install ska-low-mccs-calibration --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple

If you would like to view the source code or install from git, use::

    git clone --recurse-submodules https://gitlab.com/ska-telescope/mccs/ska-low-mccs-calibration.git

Please ensure you have all the dependency packages installed. The installation
is managed through `poetry <https://python-poetry.org/docs/>`_.
Refer to their page for instructions.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   calibration
   sky-models
   qa
   qa-plots
   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
