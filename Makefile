PROJECT = ska-low-mccs-calibration

include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/k8s.mk
include .make/helm.mk


########################################################################
# DOCS
########################################################################
DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build


########################################################################
# PYTHON
########################################################################
PYTHON_LINE_LENGTH = 88
python-post-lint:
	mypy src/ tests/

.PHONY: python-post-lint

