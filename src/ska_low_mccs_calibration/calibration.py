"""High-level Setup and Calibration Functions."""

# doesn't understand return type xarray.GainTable
# mypy: disable-error-code="no-any-return"

__all__ = [
    "calibrate_mccs_visibility",
    "adjust_gsm",
    "xy_phase_estimate",
    "rotate_gaintable",
]

import logging
from time import perf_counter
from typing import NamedTuple, Union

import numpy
import numpy.typing as npt
import scipy.constants
import xarray
from astropy.time import Time
from scipy.optimize import curve_fit
from ska_sdp_func_python.calibration.solvers import solve_gaintable

from ska_low_mccs_calibration import qa
from ska_low_mccs_calibration.eep import load_eeps, station_rotation_matrix
from ska_low_mccs_calibration.sky_model import (
    gsmap_lsm,
    gsmodel,
    predict_vis,
    solar_lsm,
)
from ska_low_mccs_calibration.utils import apply_gaintable

log = logging.getLogger("mccs-calibration-logger")


# Will break into sub-functions to deal with all of these (except too-many-statements)
# pylint: disable=too-many-statements
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
def calibrate_mccs_visibility(  # noqa: C901
    vis: xarray.Dataset,
    skymodel: Union[xarray.Dataset, str] = "gsm",
    masked_antennas: npt.ArrayLike = numpy.array([], int),
    min_uv: float = 0,
    adjust_solar_model: bool = True,
    jones_solve: bool = False,
    back_rotation: bool = False,
    refant: int = 1,
    nside: int = 32,
    dtype: npt.DTypeLike = numpy.complex64,
    ignore_eeps: bool = False,
    eep_path: str = ".",
    eep_filebase: str = "FEKO_AAVS3_vogel_256_elem_50ohm_",
    eep_rotation_deg: float = 0.0,
    eep_suffix: str = ".npy",
    niter: int = 200,
    gain_threshold: float = 0.25,
    qa_plots: Union[dict[str, bool], None] = None,
) -> tuple[
    xarray.Dataset,
    xarray.Dataset,
    xarray.Dataset,
    npt.NDArray[numpy.int_],
    NamedTuple,
]:
    r"""Calibrate MCCS visibilities against a Global Sky Model.

    :param vis: Visibility xarray to calibrate. Flags will be updated if any additional
        antenna flags are set.
    :param skymodel: Either a model visibility xarray to calibrate against or the
        name of a sky model type to generate. Supported sky models are "gsm" and "sun".
        The "gsm" model combines components of the PyGDSM GlobalSkyModel16 Galactic sky
        model that are above the horizon with a point-source solar model, while sun
        uses only the point-source solar model.
    :param ~numpy.ndarray, list masked_antennas: list of antennas that have been
        flagged as bad and should be excluded. The default is to include all antennas.
    :param min_uv: Minimum baseline length used in calibration (in metres). The
        default value is zero.
    :param adjust_solar_model: Adjust sky model after initial calibration to limit
        differences between the galactic and solar flux scales.
    :param jones_solve: Call polarised solver after initial calibration.
    :param back_rotation: Rotate the gain solution polarisation frame about zenith to
        local cardinal directions with X=East and Y=North. Default is False.
    :param refant: Antenna to use for phase referencing. Should be one of the
        zero-based indices used in the visibility dataset. Default is 1. Both
        polarisations are phase referenced against the X polarisation of refant, as a
        non-zero XY-phase is expected and solved for.
    :param nside: Healpix nside at which to generate sky model. Default is 32, which
        corresponds to approximately 110 arcminutes. This is okay at low frequencies
        but may need to be increased at higher frequencies.
    :param ~numpy.dtype dtype: The NumPy data-type to use for complex EEPs and sky
        models. The default is numpy.complex64.
    :param ignore_eeps: If True, EEP files will not be imported and station beam
        patterns and rotations will be ignored when modelling visibilities. Sky model
        components will be added with equal amplitude in XX and YY and zeros amplitude
        in XY and YX. Default is False.
    :param eep_path: Directory path to EEP image files. Default is the current dir.
    :param eep_filebase: Start of filenames, before frequency specification. Files
        are assumed to have a specific form, which for polarisation combination
        X/phi is: eep_path/eep_filebase<freq>MHz_Xpol_phi.npy. Default is
        filebase="FEKO_AAVS3_vogel_256_elem_50ohm\_".
    :param eep_rotation_deg: Azimuthal rotation of station (and therefore the patterns)
        in degrees. Default is zero degress (no rotation).
    :param eep_suffix: The suffix to use when loading EEP image files. Default is .npy.
    :param niter: Number of iterations for solver. Default 200.
    :param gain_threshold: Fractional threshold used to reject antennas with low gains.
        Any antenna with either an X or Y gain amplitude less than the median is
        flagged as bad. The default values is 0.25.
    :param qa_plots: Dictionary of QA plots to generate (or not to generate). Any
        plotting functions from :py:func:`~ska_low_mccs_calibration.qa` can be listed
        with a True or False value, and any set to true will be called with PNG output.
        Use print(ska_low_mccs_calibration.qa.__all__) for a list.
        For example qa_plots = {"generate_closure_comparison_plots": True}. Default is
        not to generate any PNG files.
    :return:
        - **gains**: GainTable Dataset containing the calibration solutions
        - **model vis**: Visibility Dataset containing model visibilities used in
          calibration. This dataset will retain any flags or weights used in
          calibration.
        - **calibrated vis**: Visibility Dataset containing calibrated visibilities.
          This dataset will retain any flags or weights used in calibration.
        - **updated antenna flags**: list of antenna flags used in final calibration
          step.
        - **calibration info**: namedtuple of return variables. See
          :py:func:`~ska_low_mccs_calibration.qa.generate_calibration_info` for details.
    """
    # Ensure that it is a numpy array
    masked_antennas = numpy.array(masked_antennas).astype(numpy.int_)

    ###############################
    # Sky model setup
    lsm_type = None
    if isinstance(skymodel, str):
        assert isinstance(skymodel, str)
        if skymodel.lower() == "gsm":
            log.info("Using a GSM with Galactic and Solar components")
            lsm_type = "gsm"
        elif skymodel.lower() == "sun":
            log.info("Using a solar point-source sky model")
            lsm_type = "sun"
        else:
            raise ValueError("calibrate_mccs_visibility: unknown sky model type")
    else:
        log.info("Solving for input sky model")
        modelvis = skymodel
        lsm_type = "input"

    ###############################
    # Other metadata
    if not numpy.iscomplexobj(numpy.array(0).astype(dtype)):
        log.warning("calibrate_mccs_visibility: Invalid dtype. Using numpy.complex64")
        dtype = numpy.complex64

    nant = len(vis.configuration.stations)

    if refant < 0 or refant >= nant:
        raise ValueError(f"calibrate_mccs_visibility: invalid refant: {refant}")

    if len(vis.frequency) > 1:
        log.warning("calibrate_mccs_visibility: Only using the first frequency")
    frequency_mhz = vis.frequency.data[0] / 1e6

    if len(vis.datetime) > 1:
        log.warning("calibrate_mccs_visibility: Only using the first time step")

    time = Time(vis.datetime.data[0])

    location = vis.configuration.location

    if vis.visibility_acc.polarisation_frame.type not in ("linear", "linearFITS"):
        raise ValueError("calibrate_mccs_visibility: require linear polarisation")
    if vis.visibility_acc.polarisation_frame.npol != 4:
        raise ValueError("calibrate_mccs_visibility: require 4 polarisations")
    xx_idx = numpy.argwhere(vis.polarisation.data == "XX")[0][0]
    xy_idx = numpy.argwhere(vis.polarisation.data == "XY")[0][0]
    yx_idx = numpy.argwhere(vis.polarisation.data == "YX")[0][0]
    yy_idx = numpy.argwhere(vis.polarisation.data == "YY")[0][0]

    # sun_elevation_deg is returned regardless of lsm_type, so pre-calculate
    [sun_azimuth_deg, sun_elevation_deg, temp_sun] = solar_lsm(
        frequency_mhz, location=location, time=time
    )

    if lsm_type != "input":

        ###############################
        # If an input visibility model was not given, generate one

        # xarray for the Local Sky Model visibilities
        modelvis = vis.copy(deep=True)
        modelvis.vis.data *= 0.0

        # also recalculate some other metadata
        index_array = numpy.stack(
            (vis.baselines.antenna1.data, vis.baselines.antenna2.data),
            axis=1,
        )
        # change function to use vis.uvw.data directly?
        p_uvw = vis.uvw.data[0] * (1e9 / scipy.constants.c)

        if not ignore_eeps:
            # Read in the AAVS3 EEPs at the nearest frequency to the observing frequency
            eeps = load_eeps(
                frequency_mhz,
                path=eep_path,
                filebase=eep_filebase,
                conjugate=True,
                suffix=eep_suffix,
                dtype=dtype,
            )
        else:
            log.info("calibrate_mccs_visibility: ignoring beam model in vis predict")
            eeps = None

        if lsm_type == "gsm":
            # Generate the diffuse galactic contribution to the local sky model
            gsmap = gsmodel(frequency_mhz, nside)[0]

            [azimuth_deg, elevation_deg, temp_lsm] = gsmap_lsm(
                gsmap, location=location, time=time
            )

            t_start = perf_counter()
            galactic_products = predict_vis(
                eeps,
                eep_rotation_deg,
                index_array,
                p_uvw.T,
                frequency_mhz,
                azimuth_deg.astype(numpy.array(0).astype(dtype).real.dtype),
                elevation_deg.astype(numpy.array(0).astype(dtype).real.dtype),
                temp_lsm,
                normalise_beam=True,
            )
            log.debug("GDSM DFT: %.2f seconds", perf_counter() - t_start)

            modelvis.vis.data[0, :, 0, xx_idx] += galactic_products[0]
            modelvis.vis.data[0, :, 0, xy_idx] += galactic_products[1]
            modelvis.vis.data[0, :, 0, yx_idx] += galactic_products[2]
            modelvis.vis.data[0, :, 0, yy_idx] += galactic_products[3]

            # Compute model contribution from the Sun

        if lsm_type in ("sun", "gsm"):
            # Generate the solar contribution to the local sky model
            if sun_elevation_deg > 0.0:
                solar_products = predict_vis(
                    eeps,
                    eep_rotation_deg,
                    index_array,
                    p_uvw.T,
                    frequency_mhz,
                    numpy.array([sun_azimuth_deg]),
                    numpy.array([sun_elevation_deg]),
                    numpy.array([temp_sun]),
                )
                modelvis.vis.data[0, :, 0, xx_idx] += solar_products[0]
                modelvis.vis.data[0, :, 0, xy_idx] += solar_products[1]
                modelvis.vis.data[0, :, 0, yx_idx] += solar_products[2]
                modelvis.vis.data[0, :, 0, yy_idx] += solar_products[3]

            else:
                log.info("calibrate_mccs_visibility: ignoring sun is below horizon")

        # Delete the EEPs
        del eeps

    ###############################
    # Apply any extra weighting or flagging

    # save a copy of the original weights and flags and reset at the end
    vis_flags_orig = vis.flags.data.copy()
    vis_weight_orig = vis.weight.data.copy()

    # Flag data below uv cutoff
    if min_uv > 0:
        mask = vis.uvw.data[0, :, 0] ** 2 + vis.uvw.data[0, :, 1] ** 2 < min_uv**2
        vis.flags.data[:, mask, :, :] = True
        modelvis.flags.data[:, mask, :, :] = True

    ###############################
    # Initial calibration of gain terms only

    # Show some extra output
    log.setLevel(logging.DEBUG)

    # Generate a single solution for the whole time range
    solution_interval = numpy.max(vis.time.data) - numpy.min(vis.time.data)

    # Compute gain solutions using default solver
    gain_solutions = solve_gaintable(
        vis=vis,
        modelvis=modelvis,
        gain_table=None,
        phase_only=False,
        niter=niter,
        tol=1e-06,
        crosspol=False,
        normalise_gains=None,
        jones_type="G",
        timeslice=solution_interval,
        refant=refant,
    )

    # Note, need to be careful if changing the weights for calibration.
    # Perhaps use use_flags=False.

    # Apply the gain solutions
    vis_calibrated = vis.copy(deep=True)
    vis_calibrated = apply_gaintable(
        vis=vis_calibrated,
        gt=gain_solutions,
        inverse=True,
    )

    ###############################
    # Reject visibilities with small gain amplitudes relative to median
    #  - could also check for abs diff from median relative to RMS...

    gain_fractional_threshold = gain_threshold

    gains_array = numpy.squeeze(gain_solutions.gain.data)

    # Mask for unflagged antennas
    good_ant_mask = numpy.isin(numpy.arange(nant), masked_antennas, invert=True)

    median_xgain = numpy.median(numpy.abs(gains_array[good_ant_mask, 0, 0]))
    median_ygain = numpy.median(numpy.abs(gains_array[good_ant_mask, 1, 1]))
    new_xflags = numpy.nonzero(
        numpy.abs(gains_array[:, 0, 0]) < median_xgain * gain_fractional_threshold
    )[0]
    new_yflags = numpy.nonzero(
        numpy.abs(gains_array[:, 1, 1]) < median_ygain * gain_fractional_threshold
    )[0]
    new_flags = numpy.unique(numpy.hstack((new_xflags, new_yflags)))
    new_unique = new_flags[numpy.isin(new_flags, masked_antennas, invert=True)]
    log.info(
        "Additional Antennas rejected based on low gains: [%s]",
        " ".join([str(ant) for ant in new_unique]),
    )
    masked_ant_orig = masked_antennas.copy()
    masked_antennas = numpy.unique(numpy.hstack((new_flags, masked_antennas)))

    # Update flags
    ant1 = vis.antenna1.data
    ant2 = vis.antenna2.data
    bl_flags = numpy.logical_or(
        numpy.isin(ant1, masked_antennas), numpy.in1d(ant2, masked_antennas)
    )
    bl_flags = numpy.logical_or(bl_flags, ant1 == ant2)
    vis_flags = numpy.tile(bl_flags, (4, 1)).T
    vis_flags_orig = numpy.logical_or(vis_flags_orig, vis_flags.reshape(1, -1, 1, 4))
    vis.flags.data = numpy.logical_or(vis.flags.data, vis_flags.reshape(1, -1, 1, 4))
    vis_calibrated.flags.data = vis.flags.data
    modelvis.flags.data = vis.flags.data

    ###############################
    # Fit for an brightness scale difference between the galaxy and the sun.
    # The change may be polarised, but would need to peel the sun away for that.
    adjustment_factor = 1.0
    if adjust_solar_model and lsm_type == "gsm":
        if sun_elevation_deg > 0.0:
            modelvis, adjustment_factor = adjust_gsm(
                vis_calibrated,
                modelvis,
                numpy.array(galactic_products),
                numpy.array(solar_products),
            )
            # Set any variable flags or weights
            modelvis.flags.data = vis.flags.data
            modelvis.weight.data = vis.weight.data

            # Redo the calibration with the updated model
            gain_solutions = solve_gaintable(
                vis=vis,
                modelvis=modelvis,
                gain_table=None,
                phase_only=False,
                niter=niter,
                tol=1e-06,
                crosspol=False,
                normalise_gains=None,
                jones_type="G",
                timeslice=solution_interval,
                refant=refant,
            )
            vis_calibrated = vis.copy(deep=True)
            vis_calibrated = apply_gaintable(
                vis=vis_calibrated,
                gt=gain_solutions,
                inverse=True,
            )

    ###############################
    # Polarisation calibration

    xy_phase = 0.0

    if jones_solve:
        # Run an antenna-based polarised solver
        t_start = perf_counter()
        gain_solutions = solve_gaintable(
            vis=vis,
            modelvis=modelvis,
            solver="normal_equations",
            niter=niter,
            gain_table=gain_solutions,
            phase_only=False,
            tol=1e-06,
            crosspol=False,
            normalise_gains=None,
            jones_type="G",
            timeslice=solution_interval,
        )
        log.info("Solving: %.2f seconds", perf_counter() - t_start)

        vis_calibrated = vis.copy(deep=True)
        vis_calibrated = apply_gaintable(
            vis=vis_calibrated,
            gt=gain_solutions,
            inverse=True,
        )

        # Reference all phases to the phase of a single polarisation: J[refant][0,0]
        refphase = numpy.angle(gain_solutions.gain.data[0, refant, 0, 0, 0])
        gain_solutions.gain.data *= numpy.exp(-1j * refphase)

        xy_phase = xy_phase_estimate(vis_calibrated, modelvis)
        log.info("Estimated XY-phase: %.2f deg (ignored)", xy_phase * 180.0 / numpy.pi)

    else:
        # Estimate XY phase by comparing calibrated and model visibility cross terms
        xy_phase = xy_phase_estimate(vis_calibrated, modelvis)
        log.info("Estimated XY-phase: %.2f deg", xy_phase * 180.0 / numpy.pi)

        xy_correction = numpy.exp(1j * xy_phase)
        # The y phase is ahead of x by xy_phase, so add to the y gain
        # The off-diagonal term should be zero, so ignore it
        gain_solutions.gain.data[:, :, :, 1, 1] *= xy_correction
        # Subtract phase shifts from corrected data (double -ve when y is conjugated)
        vis_calibrated.vis.data[:, :, :, xy_idx] *= xy_correction
        vis_calibrated.vis.data[:, :, :, yx_idx] *= xy_correction.conj()

    if back_rotation:
        # Rotate the X and Y polarisation axes from the standard X=East, Y=North.
        # Don't update vis_calibrated unless we intend to update modelvis as well.
        gain_solutions = rotate_gaintable(gain_solutions, eep_rotation_deg)

    masked_ant_final = numpy.array(masked_antennas).astype(numpy.int_)

    calibration_info = qa.generate_calibration_info(
        vis=vis_calibrated,
        modelvis=modelvis,
        masked_antennas_init=masked_ant_orig,
        masked_antennas_final=masked_ant_final,
        location=location,
        time=time,
        xy_phase=xy_phase,
        sun_adjustment_factor=adjustment_factor,
    )

    # Remove any temporary flags and weights.
    vis.flags.data = vis_flags_orig
    vis.weight.data = vis_weight_orig

    # Create QA plots, if requested.
    save_qa_plots(qa_plots, gain_solutions, modelvis, vis_calibrated, masked_ant_final)

    return gain_solutions, modelvis, vis_calibrated, masked_ant_final, calibration_info


def add_sky_components(
    sky_components: tuple[
        npt.NDArray[Union[numpy.complex64, numpy.complex128]],
        npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    ],
    gain_factor_a: float,
    gain_factor_b: float,
) -> npt.NDArray[Union[numpy.complex64, numpy.complex128]]:
    """Optimisation function for scipy curve_fit call in adjust_gsm.

    Returns gain_factor_a * sky_components[0] + gain_factor_b * sky_components[1]

    :param sky_components: list containing model data for two sky components
    :param gain_factor_a: multiplication factor for the first sky component
    :param gain_factor_b: multiplication factor for the second sky component
    :return: model data with updated sky component combination
    """
    comp_a, comp_b = sky_components
    return gain_factor_a * comp_a + gain_factor_b * comp_b


def adjust_gsm(
    vis: xarray.Dataset,
    modelvis: xarray.Dataset,
    gal_data: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    sun_data: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
) -> tuple[xarray.Dataset, float]:
    """Fit for a brightness scale difference between the galaxy and the sun.

    The change may be polarised, but would need to peel the sun away for that.

    :param vis: Calibrated Visibility xarray
    :param modelvis: Model Visibility xarray to update
    :param gal_data: 2D NumPy ndarray containing galactic contribution to model vis
    :param sun_data: 2D NumPy ndarray containing solar contribution to model vis
    :return: (updated modelvis, adjustment factor)
    """
    # Ensure that these are numpy arrays
    gal_data = numpy.array(gal_data)
    sun_data = numpy.array(sun_data)

    xx_idx = numpy.argwhere(vis.polarisation.data == "XX")[0][0]
    xy_idx = numpy.argwhere(vis.polarisation.data == "XY")[0][0]
    yx_idx = numpy.argwhere(vis.polarisation.data == "YX")[0][0]
    yy_idx = numpy.argwhere(vis.polarisation.data == "YY")[0][0]

    mask = numpy.logical_not(numpy.any(vis.flags.data[0, :, 0, :], axis=-1))

    # curve_fit casts to real, so just use the real part
    [gal_factor, sun_factor] = curve_fit(
        add_sky_components,
        (gal_data[xx_idx, mask].real, sun_data[xx_idx, mask].real),
        vis.vis.data[0, mask, 0, xx_idx].real,
        (1.0, 1.0),
    )[0]

    scale = sun_factor / gal_factor
    log.info("adjust_gsm: Resetting sky model with sun * %.2f", scale)

    modelvis.vis.data[0, :, 0, xx_idx] = gal_data[0] + scale * sun_data[0]
    modelvis.vis.data[0, :, 0, xy_idx] = gal_data[1] + scale * sun_data[1]
    modelvis.vis.data[0, :, 0, yx_idx] = gal_data[2] + scale * sun_data[2]
    modelvis.vis.data[0, :, 0, yy_idx] = gal_data[3] + scale * sun_data[3]

    return modelvis, scale


def xy_phase_estimate(vis: xarray.Dataset, modelvis: xarray.Dataset) -> float:
    """Estimate uncalibrated xy-phase by comparing calibrated and model visibilities.

    :param vis: Calibrated Visibility xarray
    :param modelvis: Model Visibility xarray to update
    :return: xy-phase estimate (radians)
    """
    xy_idx = numpy.argwhere(vis.polarisation.data == "XY")[0][0]
    yx_idx = numpy.argwhere(vis.polarisation.data == "YX")[0][0]

    mask = numpy.logical_not(numpy.any(vis.flags.data[0, :, 0, :], axis=-1))

    cal_vis = vis.vis.data[0, mask, 0, :]
    mdl_vis = modelvis.vis.data[0, mask, 0, :]

    angle_diff1 = numpy.angle(mdl_vis[:, xy_idx]) - numpy.angle(cal_vis[:, xy_idx])
    angle_diff2 = numpy.angle(mdl_vis[:, yx_idx]) - numpy.angle(cal_vis[:, yx_idx])

    xy_phase1 = numpy.angle(numpy.sum(numpy.exp(1j * angle_diff1)))
    xy_phase2 = -numpy.angle(numpy.sum(numpy.exp(1j * angle_diff2)))
    xy_phase = numpy.angle(
        numpy.mean([numpy.exp(1j * xy_phase1), numpy.exp(1j * xy_phase2)])
    )

    return xy_phase.astype(numpy.float_)


def rotate_gaintable(
    gaintable: xarray.Dataset,
    eep_rotation_deg: float = 0.0,
) -> xarray.Dataset:
    """Rotate the X and Y polarisation axes from the nominal X=East, Y=North.

    The station being calibrated will in general have an azimuthal orientation that is
    different from other stations. As both the observed and model visibilities have
    polarisation axes in the rotated frame, the calibration solutions generated will be
    as well. This function rotates the gain solution matrices relative to the standard
    X=East, Y=North frame. When the solution matrices are inverted and applied to the
    observed data, the resulting calibrated visibilities will be back-rotated to the
    standard frame.

    The direction-independent calibration matrices dealt with in this package are
    solved on the left-hand side of the beam matrices, as the effects are assumed to
    occur in the system after passing through the antennas. As such, the rotation
    matrix should be applied on the right-hand side of the calibration matrices.

    :param gaintable: ska-sdp-datamodels GainTable xarray containing gain solutions.
    :param eep_rotation_deg: rotation of the station in degrees. Default is 0.
    :return: update gaintable.
    """
    if gaintable.gain.shape[-1] != 2 or gaintable.gain.shape[-2] != 2:
        raise ValueError("rotate_gaintable: rotation requires 2x2 Jones matrices")

    Jcal = gaintable.gain.data.copy()
    Jrot = station_rotation_matrix(eep_rotation_deg, reverse=False)
    gaintable.gain.data = numpy.einsum("tbfxp,py->tbfxy", Jcal, Jrot)

    return gaintable


def save_qa_plots(
    qa_plots: Union[dict[str, bool], None],
    gain_table: xarray.Dataset,
    model_vis: xarray.Dataset,
    calibrated_vis: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
) -> None:
    """Save user-selected QA plots to disk.

    :param qa_plots: Dictionary of QA plots to generate (or not to generate). Any
        plotting functions from :py:func:`~ska_low_mccs_calibration.qa` can be listed
        with a True or False value, and any set to true will be called with PNG output.
    """
    if qa_plots is None:
        return

    # for key in qa_plots:
    #     print(f"{key}: {qa_plots[key]}")

    # Gains solutions as functions of antenna indices
    if qa_plots.get("generate_gaintable_plots", False):
        fig = qa.generate_gaintable_plots(gain_table, masked_antennas)
        fig.savefig("gaintable_plots.png")

    # Gains solutions spatial scatter plots
    if qa_plots.get("generate_gaintable_scatter_plots", False):
        # Don't have easy access to ENU data, so leave this unless it is needed.
        log.warning("gaintable_scatter_plots are not currently supported.")

    # Calibrated and model visibilities as functions of baseline length
    if qa_plots.get("generate_visibility_plots", False):
        fig = qa.generate_visibility_plots(calibrated_vis, model_vis, masked_antennas)
        fig.savefig("visibility_plots.png")

    # Calibrated versus model visibility scatter plots
    if qa_plots.get("generate_visibility_comparison_plots", False):
        fig = qa.generate_visibility_comparison_plots(
            calibrated_vis, model_vis, masked_antennas
        )
        fig.savefig("visibility_comparison_plots.png")

    # Calibrated versus model visibility scatter plots
    if qa_plots.get("generate_closure_comparison_plots", False):
        fig = qa.generate_closure_comparison_plots(
            calibrated_vis, model_vis, masked_antennas, ctype="uvmax"
        )
        fig.savefig("closure_comparison_plots.png")

    return
