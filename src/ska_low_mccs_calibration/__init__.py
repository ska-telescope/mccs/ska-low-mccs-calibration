"""Top-level package for ska-low-mccs-calibration."""

__author__ = "Drew Devereux"
__email__ = "drew.devereux@skao.int"
__version__ = "0.0.0"
