"""Functions for gathering or generating quality assurance information."""

__all__ = [
    "generate_calibration_info",
    "generate_gaintable_plots",
    "generate_gaintable_scatter_plots",
    "generate_visibility_plots",
    "generate_visibility_comparison_plots",
    "generate_closure_comparison_plots",
]

import logging
from collections import namedtuple
from typing import Any, NamedTuple, Optional, Union

import matplotlib.figure
import matplotlib.pyplot as plt
import numpy
import numpy.typing as npt
import scipy.constants
import xarray
from astropy import units
from astropy.coordinates import AltAz, Angle, SkyCoord, get_sun
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

log = logging.getLogger("mccs-calibration-qa-logger")


# pylint: disable=too-many-arguments
def generate_calibration_info(
    vis: xarray.Dataset,
    modelvis: xarray.Dataset,
    masked_antennas_init: npt.NDArray[numpy.int_],
    masked_antennas_final: npt.NDArray[numpy.int_],
    location: units.Quantity,
    time: units.Quantity,
    xy_phase: float,
    sun_adjustment_factor: float,
) -> NamedTuple:
    """Collect calibration QA metadata and return as a namedtuple.

    :param vis: Calibrated Visibility xarray
    :param modelvis: Model Visibility xarray to update
    :param masked_antennas_init: initial list of flagged antenna
    :param masked_antennas_final: final list of flagged antenna
    :param location: Astropy coordinates of the observation location.
    :param time: Astropy time of the observation.
    :param xy_phase: Any excess XY-phase determined after calibration.
    :param sun_adjustment_factor: Amplitude factor determined for the sun temperature.
    :return:
        - **corrcoeff**: List of Pearson product-moment correlation coefficients for
          each polarisation of the calibrated visibilities and the model.
        - **residual_std**: List of residual visibility standard deviation per
          polarisation (K).
        - **residual_max**: List of residual visibility maximum absolution deviation
          per polarisation (K).
        - **xy_phase**: Estimated xy-phase error (degrees).
        - **n_masked_initial**: Initial number of masked antennas.
        - **n_masked_final**: Final number of masked antennas.
        - **lst**: Apparent sidereal time at station (degrees).
        - **galactic_centre_elevation**: Elevation of the galactic centre (degrees).
        - **sun_elevation**: Elevation of the sun (degrees).
        - **sun_adjustment_factor**: Solar adjustment factor (if
          *adjust_solar_model=True*, "1" otherwise).
    """
    mask = numpy.invert(modelvis.flags.data[0, :, 0, 0])
    cal_data = vis.vis.data[0, mask, 0]
    mdl_data = modelvis.vis.data[0, mask, 0]
    corrcoeff = generate_corrcoeff(cal_data, mdl_data)
    residual_std = [
        numpy.std(cal_data[:, 0] - mdl_data[:, 0]),
        numpy.std(cal_data[:, 1] - mdl_data[:, 1]),
        numpy.std(cal_data[:, 2] - mdl_data[:, 2]),
        numpy.std(cal_data[:, 3] - mdl_data[:, 3]),
    ]
    residual_max = [
        numpy.max(numpy.abs(cal_data[:, 0] - mdl_data[:, 0])),
        numpy.max(numpy.abs(cal_data[:, 1] - mdl_data[:, 1])),
        numpy.max(numpy.abs(cal_data[:, 2] - mdl_data[:, 2])),
        numpy.max(numpy.abs(cal_data[:, 3] - mdl_data[:, 3])),
    ]

    calibration_info = namedtuple(
        "calibration_info",
        [
            "corrcoeff",
            "residual_std",
            "residual_max",
            "xy_phase",
            "n_masked_initial",
            "n_masked_final",
            "lst",
            "galactic_centre_elevation",
            "sun_elevation",
            "sun_adjustment_factor",
        ],
    )
    return calibration_info(
        corrcoeff,
        residual_std,
        residual_max,
        xy_phase * 180.0 / numpy.pi,
        len(masked_antennas_init),
        len(masked_antennas_final),
        time.sidereal_time("apparent", location).value,
        SkyCoord(
            l=Angle(0.0, unit="degree"),
            b=Angle(0.0, unit="degree"),
            frame="galactic",
        )
        .transform_to(AltAz(obstime=time, location=location))
        .alt.degree,
        get_sun(time).transform_to(AltAz(obstime=time, location=location)).alt.degree,
        sun_adjustment_factor,
    )


def generate_corrcoeff(
    vis: npt.NDArray[numpy.complex_],
    mdl: npt.NDArray[numpy.complex_],
) -> list["float"]:
    """
    Generate Pearson product-moment correlation coefficients.

    :param vis: array of visibilities.
    :param mdl: array of model visibilities.
    :return: List of Pearson product-moment correlation coefficients for each
          polarisation of the calibrated visibilities and the model.
    """
    xxcoeff = xycoeff = yxcoeff = yycoeff = 0.0
    if not numpy.all(vis[:, 0] == mdl[:, 0]):
        xxcoeff = numpy.corrcoef(numpy.abs(vis[:, 0]), numpy.abs(mdl[:, 0]))[0, 1]
    if not numpy.all(vis[:, 1] == mdl[:, 1]):
        xycoeff = numpy.corrcoef(numpy.abs(vis[:, 1]), numpy.abs(mdl[:, 1]))[0, 1]
    if not numpy.all(vis[:, 2] == mdl[:, 2]):
        yxcoeff = numpy.corrcoef(numpy.abs(vis[:, 2]), numpy.abs(mdl[:, 2]))[0, 1]
    if not numpy.all(vis[:, 3] == mdl[:, 3]):
        yycoeff = numpy.corrcoef(numpy.abs(vis[:, 3]), numpy.abs(mdl[:, 3]))[0, 1]
    return [xxcoeff, xycoeff, yxcoeff, yycoeff]


def generate_gaintable_plots(
    gain_table: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
) -> matplotlib.figure.Figure:
    """Plot the amplitude and phase of the Jones matrix elements versus antenna.

    :param gain_table: GainTable xarray dataset containing calibration solutions.
        Assumes a single time and frequency.
    :param masked_antennas: 1D numpy array of flagged antenna indices.
    :return: matplotlib figure of Jones matrix amp and phase versus antenna number.
    """
    if (gain_table.receptor_frame1 != gain_table.receptor_frame2) or (
        gain_table.receptor_frame1.type.find("linear") != 0
    ):
        raise ValueError("Expect 2x2 Jones matrices in the linear frame")

    if len(gain_table.time) > 1:
        log.warning("Expecting a single time step. Only plotting the first")
    if len(gain_table.frequency) > 1:
        log.warning("Expecting a single frequency channel. Only plotting the first")

    nant = len(gain_table.antenna)
    mask = numpy.isin(numpy.arange(nant), masked_antennas, invert=True)
    ant_list = numpy.arange(nant)[mask]

    gain = numpy.abs(gain_table.gain.data[0, mask, 0])
    phase = numpy.angle(gain_table.gain.data[0, mask, 0], deg=True)

    fig, axs = plt.subplots(2, 2, figsize=[12, 12])
    if not isinstance(axs, numpy.ndarray):  # keep mypy happy
        raise ValueError("Expected array of subplots")

    msize = 5.0
    tfsize = 14

    ymin = -0.05 * numpy.max(gain)
    ymax = 1.05 * numpy.max(gain)

    ax = axs[0, 0]
    ax.plot(ant_list, gain[:, 0, 0], "b.", markersize=msize, label="J[0,0]=gX")
    ax.plot(ant_list, gain[:, 1, 1], "r.", markersize=msize, label="J[1,1]=gY")
    ax.set_title("Jones diagonlal amplitude", fontsize=tfsize)
    ax.set_xlabel("Antenna EEP index")
    ax.set_ylabel("Amplitude")
    ax.set_ylim(ymin=ymin, ymax=ymax)
    ax.grid()
    ax.legend(loc=4)

    ax = axs[0, 1]
    ax.plot(ant_list, gain[:, 0, 1], "c.", markersize=msize, label="J[0,1]=gX.dXY")
    ax.plot(ant_list, gain[:, 1, 0], "m.", markersize=msize, label="J[1,0]=gY.dYX")
    ax.set_title("Jones off-diagonlal amplitude", fontsize=tfsize)
    ax.set_xlabel("Antenna EEP index")
    ax.set_ylim(ymin=ymin, ymax=ymax)
    ax.grid()
    ax.legend(loc=1)

    ax = axs[1, 0]
    ax.plot(ant_list, phase[:, 0, 0], "b.", markersize=msize, label="J[0,0]=gX")
    ax.plot(ant_list, phase[:, 1, 1], "r.", markersize=msize, label="J[1,1]=gY")
    ax.set_title("Jones diagonlal phase", fontsize=tfsize)
    ax.set_xlabel("Antenna EEP index")
    ax.set_ylabel("Phase (degrees)")
    ax.grid()

    ax = axs[1, 1]
    ax.plot(ant_list, phase[:, 0, 1], "c.", markersize=msize, label="J[0,1]=gX.dXY")
    ax.plot(ant_list, phase[:, 1, 0], "m.", markersize=msize, label="J[1,0]=gY.dYX")
    ax.set_title("Jones off-diagonlal phase", fontsize=tfsize)
    ax.set_xlabel("Antenna EEP index")
    ax.grid()

    return fig


def generate_gaintable_scatter_plots(
    gain_table: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
    enu: npt.NDArray[numpy.float_],
    refant: Optional[int] = None,
) -> matplotlib.figure.Figure:
    """Plot the amplitude and phase of the Jones matrix elements versus position.

    :param gain_table: GainTable xarray dataset containing calibration solutions.
        Assumes a single time and frequency.
    :param masked_antennas: 1D numpy array of flagged antenna indices.
    :param ~numpy.ndarray, enu: Array of antenna East,North,Up coordinates.
    :param refant: Index of reference antenna to use in phase plots. None by default.
    :return: matplotlib figure of Jones matrix amp and phase versus antenna position.
    """
    if len(gain_table.time) > 1:
        log.warning("Expecting a single time step. Only plotting the first")
    if len(gain_table.frequency) > 1:
        log.warning("Expecting a single frequency channel. Only plotting the first")

    mask = numpy.isin(
        numpy.arange(len(gain_table.antenna)), masked_antennas, invert=True
    )

    phase = numpy.angle(gain_table.gain.data[0, mask, 0], deg=True)

    fig, axs = plt.subplots(
        2, len(gain_table.receptor1) * len(gain_table.receptor2), figsize=[12, 9]
    )
    if not isinstance(axs, numpy.ndarray):  # keep mypy happy
        raise ValueError("Expected array of subplots")

    k = 0
    for i, pol1 in enumerate(gain_table.receptor1.data):
        for j, pol2 in enumerate(gain_table.receptor2.data):

            ax = axs[0, k]
            sca = ax.scatter(
                enu[mask, 0],
                enu[mask, 1],
                c=numpy.abs(gain_table.gain.data[0, mask, 0, i, j]),
            )
            ax.set_aspect("equal")
            ax.set_title(f"abs(J[{pol1},{pol2}])")
            if k == 0:
                ax.set_ylabel("N (m)")
            ax.grid()
            fig.colorbar(sca, ax=ax, orientation="horizontal")

            ax = axs[1, k]
            phase = numpy.angle(gain_table.gain.data[0, mask, 0, i, j], deg=True)
            if refant is not None:
                phase -= numpy.angle(gain_table.gain.data[0, refant, 0, i, j], deg=True)
                phase = numpy.mod(phase, 360)
                phase -= 360 * (phase > 180)
            sca = ax.scatter(enu[mask, 0], enu[mask, 1], c=phase)
            ax.set_aspect("equal")
            ax.set_title(f"phase(J[{pol1},{pol2}])")
            ax.set_xlabel("E (m)")
            if k == 0:
                ax.set_ylabel("N (m)")
            ax.grid()
            fig.colorbar(sca, ax=ax, orientation="horizontal")

            k += 1

    return fig


def generate_visibility_plots(
    vis: xarray.Dataset,
    modelvis: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
) -> matplotlib.figure.Figure:
    """Plot calibrated and model visibilities.

    :param vis: Visibility xarray dataset containing calibrated data.
        Assumes a single time and frequency.
    :param modelvis: Visibility xarray dataset containing model data
    :param masked_antennas: 1D numpy array of flagged antenna indices
    :return: matplotlib figure of visibility amplitude and phase.
    """
    # update vis flags
    bl_flags = numpy.any(vis.flags.data, axis=(0, 2, 3))
    bl_flags = numpy.logical_or(bl_flags, vis.antenna1.data == vis.antenna2.data)
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna1.data, masked_antennas)
    )
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna2.data, masked_antennas)
    )

    uvw = vis.uvw.data[0, ~bl_flags, :]
    wl = scipy.constants.c / vis.frequency.data[0]
    uvdist = 1.0 / wl * numpy.sqrt(uvw[:, 0] ** 2 + uvw[:, 1] ** 2)

    pol_list = vis.polarisation.data

    fig, axs = plt.subplots(3 * len(pol_list), 2, figsize=[11, 16])
    if not isinstance(axs, numpy.ndarray):  # keep mypy happy
        raise ValueError("Expected array of subplots")

    for ax_id, pol in enumerate(pol_list):

        polid = numpy.argwhere(vis.polarisation.data == pol)[0][0]
        visdata = vis.vis.data[0, ~bl_flags, 0, polid]
        mdldata = modelvis.vis.data[0, ~bl_flags, 0, polid]

        _update_visibility_plot_rows(
            axs, ax_id, pol, visdata, mdldata, uvdist, vis.frequency.data[0]
        )

    return fig


def _update_visibility_plot_rows(
    axs: npt.NDArray[Any],  # matplotlib.axes.Axes
    ax_id: int,
    pol: str,
    visdata: npt.NDArray[numpy.complex_],
    mdldata: npt.NDArray[numpy.complex_],
    uvdist: npt.NDArray[numpy.float_],
    freq: float,
) -> None:
    """Update polarisation panels for generate_visibility_plots.

    Helper function to deal with graphics for one polarisation.

    :param axs: numpy array of matplotlib Axes objects. There are three rows of panels
        per polarisation and two columns of panels for amplitude and phase.
    :param ax_id: axis indexing parameter for this polarisation.
    :param pol: polarisation label.
    :param visdata: 1D array of visibilities.
    :param mdldata: 1D array of model visibilities.
    :param uvdist: 1D array of baseline lengths.
    :param freq: frequency in Hz.
    """
    msize = 0.15

    ymax = max(numpy.max(numpy.abs(visdata)), numpy.max(numpy.abs(mdldata)))

    ax = axs[3 * ax_id, 0]
    ax.plot(uvdist, numpy.abs(visdata), "b*", markersize=msize, label=f"{pol}")
    if ax_id == 0:
        ax.set_title("Visibility amplitude (K)", fontsize=14)
    ax.set_ylim([0.0, ymax])
    ax.axes.xaxis.set_ticklabels([])
    ax.grid()
    ax.legend(loc=1)

    ax = axs[3 * ax_id, 1]
    ax.plot(uvdist, numpy.angle(visdata, deg=True), "b*", markersize=msize)
    ax.set_ylim([-200, 200])
    if ax_id == 0:
        ax.set_title("Visibility phase (deg)", fontsize=14)
    ax.axes.xaxis.set_ticklabels([])
    ax.grid()

    ax = axs[3 * ax_id + 1, 0]
    ax.plot(uvdist, numpy.abs(mdldata), "g*", markersize=msize, label=f"{pol} model")
    ax.set_ylim([0.0, ymax])
    ax.axes.xaxis.set_ticklabels([])
    ax.grid()
    ax.legend(loc=1)

    ax = axs[3 * ax_id + 1, 1]
    ax.plot(uvdist, numpy.angle(mdldata, deg=True), "g*", markersize=msize)
    ax.set_ylim([-200, 200])
    ax.axes.xaxis.set_ticklabels([])
    ax.grid()

    ax = axs[3 * ax_id + 2, 0]
    ax.plot(
        uvdist,
        numpy.abs(visdata - mdldata),
        "m*",
        markersize=msize,
        label=f"{pol} residual, std = {numpy.std(visdata - mdldata):.1f} K",
    )
    ax.set_ylim([0.0, ymax])
    if ax_id < 3:
        ax.axes.xaxis.set_ticklabels([])
    else:
        ax.set_xlabel(f"uvdist (wavelengths at {freq / 1e6:.0f} MHz)")
    ax.grid()
    ax.legend(loc=1)

    ax = axs[3 * ax_id + 2, 1]
    angle_diff = numpy.angle(visdata, deg=True) - numpy.angle(mdldata, deg=True)
    angle_diff += 360 * (angle_diff < -180)
    angle_diff -= 360 * (angle_diff > +180)
    ax.plot(
        uvdist,
        angle_diff,
        "m*",
        markersize=msize,
        label=rf"phase diff std = {numpy.std(angle_diff):.1f}$^\circ$",
    )
    ax.set_ylim([-200, 200])
    if ax_id < 3:
        ax.axes.xaxis.set_ticklabels([])
    else:
        ax.set_xlabel(f"uvdist (wavelengths at {freq / 1e6:.0f} MHz)")
    ax.grid()
    ax.legend(loc=1)


def generate_visibility_comparison_plots(
    vis: xarray.Dataset,
    modelvis: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
) -> matplotlib.figure.Figure:
    """Plot calibrated and model visibility comparisons.

    :param vis: Visibility xarray dataset containing calibrated data.
        Assumes a single time and frequency.
    :param modelvis: Visibility xarray dataset containing model data
    :param masked_antennas: 1D numpy array of flagged antenna indices
    :return: matplotlib figure of visibility comparisons.
    """
    # update vis flags
    bl_flags = numpy.any(vis.flags.data, axis=(0, 2, 3))
    bl_flags = numpy.logical_or(bl_flags, vis.antenna1.data == vis.antenna2.data)
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna1.data, masked_antennas)
    )
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna2.data, masked_antennas)
    )

    pol_list = vis.polarisation.data

    fig, axs = plt.subplots(3, len(pol_list), figsize=[14, 12])
    if not isinstance(axs, numpy.ndarray):  # keep mypy happy
        raise ValueError("Expected array of subplots")

    for ax_id, pol in enumerate(pol_list):
        polid = numpy.argwhere(vis.polarisation.data == pol)[0][0]
        _update_visibility_comparison_plot_rows(
            axs,
            ax_id,
            pol,
            vis.vis.data[0, ~bl_flags, 0, polid],
            modelvis.vis.data[0, ~bl_flags, 0, polid],
        )

    return fig


def _update_visibility_comparison_plot_rows(
    axs: npt.NDArray[Any],  # matplotlib.axes.Axes
    ax_id: int,
    pol: str,
    visdata: npt.NDArray[numpy.complex_],
    mdldata: npt.NDArray[numpy.complex_],
) -> None:
    """Update polarisation panels for generate_visibility_comparison_plots.

    Helper function to deal with graphics for one polarisation.

    :param axs: numpy array of matplotlib Axes objects. There are three rows of panels
        (amplitude, phase and phase histogram) and one column of panels per
        polarisation.
    :param ax_id: axis indexing parameter for this polarisation.
    :param pol: polarisation label.
    :param visdata: 1D array of visibilities.
    :param mdldata: 1D array of model visibilities.
    """
    msize = 0.25
    ymax = max(numpy.max(numpy.abs(visdata)), numpy.max(numpy.abs(mdldata)))

    ax = axs[0, ax_id]
    ax.plot(numpy.abs(mdldata), numpy.abs(visdata), "b*", markersize=msize)
    ax.set_title(f"{pol} amplitude", fontsize=12)
    ax.set_xlabel("model amplitude")
    if ax_id == 0:
        ax.set_ylabel("vis amplitude")
    ax.set_xlim([0, ymax])
    ax.set_ylim([0, ymax])
    ax.set_aspect("equal")
    ax.grid()

    ax = axs[1, ax_id]
    vis_angle = numpy.angle(visdata, deg=True)
    model_angle = numpy.angle(mdldata, deg=True)
    angle_diff = vis_angle - model_angle
    vis_angle += 360 * (angle_diff < -180)
    vis_angle -= 360 * (angle_diff > +180)
    angle_diff += 360 * (angle_diff < -180)
    angle_diff -= 360 * (angle_diff > +180)
    ax.plot(model_angle, vis_angle, "b*", markersize=msize)
    ax.set_title(f"{pol} phase", fontsize=12)
    ax.set_xlabel("model phase (deg)")
    if ax_id == 0:
        ax.set_ylabel("vis phase (deg)")
    ax.set_xlim([-180, 180])
    ax.set_ylim([-180, 180])
    ax.set_aspect("equal")
    ax.grid()

    ax = axs[2, ax_id]
    ax.set_title(f"{pol} phase residual histo", fontsize=12)
    ax.set_xlabel("Phase bin (degrees)")
    if ax_id == 0:
        ax.set_ylabel("Count")
    ax.hist(
        angle_diff,
        bins=range(-180, 181, 3),
        histtype="step",
        color="b",
        label=rf"std: {numpy.std(angle_diff):.1f}$^\circ$",
    )
    ax.legend(loc=1)


def generate_closure_comparison_plots(
    vis: xarray.Dataset,
    modelvis: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
    min_uv: float = 0.0,
    decimate: int = 1,
    ctype: Optional[str] = None,
) -> matplotlib.figure.Figure:
    """Plot visibility closure relation comparisons against model visibilities.

    Talk about there being too many cross products and it being advisable to generate
    and display products for sub-arrays at a time.

    :param vis: Visibility xarray dataset containing calibrated data.
        Assumes a single time and frequency.
    :param modelvis: Visibility xarray dataset containing model data
    :param masked_antennas: 1D numpy array of flagged antenna indices
    :param min_uv: Minimum baseline length allowed in triplets, in metres. Default = 0.
    :param decimate: Use a sub-array formed from every _decimate_ antennas to speed up
        plotting. Default is 1 (no decimation).
    :param ctype: What to use for colouring. Allowed options are "uvmin", which will
        set colours based on the minimum baseline length in each triplet, "uvmax",
        which will set colours based on the maximum baseline length in each triplet,
        and "uvdiff", which will set colours based on uvmax - uvmin.
        Default is none, which will use a single colour for all points.
    :return: matplotlib figure of visibility comparisons.
    """
    if vis.configuration.receptor_frame.type.find("linear") != 0:
        raise ValueError("Expect linear polarisation for visibilities")
    pol_ids = [
        numpy.argwhere(vis.polarisation.data == "XX")[0][0],
        numpy.argwhere(vis.polarisation.data == "YY")[0][0],
    ]

    bl_flags = _generate_closure_comparison_plot_bl_flags(
        vis, masked_antennas, decimate
    )

    bl_ids = numpy.array([vis.antenna1.data[~bl_flags], vis.antenna2.data[~bl_flags]])
    nant = numpy.max(bl_ids) + 1

    fig, axs = plt.subplots(len(pol_ids), 3, figsize=[16, 12])
    if not isinstance(axs, numpy.ndarray):  # keep mypy happy
        raise ValueError("Expected array of subplots")

    # Correlation matrices are used below to simplify indexing (see vismat), with
    # matrix columns used to select groups of third antennas for baseline pairs. Set up
    # a mask to avoid duplicate triplets by only selecting columns with larger indices.
    triple_mask = _generate_closure_comparison_plot_mask(
        vis.uvw.data[0, ~bl_flags, :], bl_ids[0], bl_ids[1], nant, min_uv
    )

    # Find the min or max baseline in each triple for scatter plot colour
    colour = _generate_closure_comparison_plot_colours(
        vis.uvw.data[0, ~bl_flags, :], bl_ids[0], bl_ids[1], nant, ctype
    )

    for _, polid in enumerate(pol_ids):
        _update_closure_comparison_plot_rows(
            axs,
            _,
            vis.polarisation.data[polid],
            vis.vis.data[0, ~bl_flags, 0, polid],
            modelvis.vis.data[0, ~bl_flags, 0, polid],
            bl_ids,
            triple_mask,
            colour,
            ctype,
        )

    return fig


def _generate_closure_comparison_plot_bl_flags(
    vis: xarray.Dataset,
    masked_antennas: npt.NDArray[numpy.int_],
    decimate: int,
) -> Union[npt.NDArray[numpy.bool_], Any]:
    """Generate flags for closure plot baselines.

    Helper function to set baseline flags for closure plots.

    :param vis: Visibility xarray dataset containing calibrated data.
        Assumes a single time and frequency.
    :param masked_antennas: 1D numpy array of flagged antenna indices.
    :param decimate: Use a sub-array formed from every _decimate_ antennas to speed up
        plotting.
    :return: 1D array of baseline flags (length = vis.vis.shape[1]).
    """
    if decimate > 1:
        # If decimating, add flags for all antennas that aren't in the decimated list
        nant = len(vis.configuration.id)
        ants = numpy.arange(nant)
        masked_antennas = numpy.unique(
            numpy.append(
                masked_antennas,
                ants[numpy.isin(ants, numpy.arange(0, nant, decimate), invert=True)],
            )
        )

    # update vis flags
    bl_flags = numpy.any(vis.flags.data, axis=(0, 2, 3))
    bl_flags = numpy.logical_or(bl_flags, vis.antenna1.data == vis.antenna2.data)
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna1.data, masked_antennas)
    )
    bl_flags = numpy.logical_or(
        bl_flags, numpy.isin(vis.antenna2.data, masked_antennas)
    )

    return bl_flags


def _generate_closure_comparison_plot_mask(
    uvw: npt.NDArray[numpy.float_],
    stn1: npt.NDArray[numpy.int_],
    stn2: npt.NDArray[numpy.int_],
    nant: int,
    min_uv: float,
) -> Union[npt.NDArray[numpy.bool_], Any]:
    """Generate masks for closure plot triplets.

    Helper function to generate a mask for unique closure triplets. For each baseline,
    triplets are generated by selecting all columns for the two relevant rows of the
    correlation matrix. This leads to duplicate triplets, so set a mask for unique
    elements.

    :param uvw: 2D array of uvw coordinates [nvis, 3].
    :param stn1: 1D array of baseline antenna indices (first antenna) [nvis].
    :param stn2: 1D array of baseline antenna indices (second antenna) [nvis].
    :param nant: number of antennas included in correlation matrix.
    :return: 1D mask of unique closure triplets [nvis * nant].
    """
    # Correlation matrices are used below to simplify indexing (see vismat), with
    # matrix columns used to select groups of third antennas for baseline pairs. Set up
    # a mask to avoid duplicate triplets by only selecting columns with larger indices.
    stnmax = numpy.max(numpy.stack([stn1, stn2]), axis=0)
    # only include third antennas with indices larger than the max baseline index
    triple_mask = (
        stnmax[:, numpy.newaxis] - numpy.arange(nant)[numpy.newaxis, :] < 0
    ).reshape(-1)
    # Also include a minimum baseline length mask for baselines in triple products?
    if min_uv > 0:
        mask = uvw[:, 0] ** 2 + uvw[:, 1] ** 2 > min_uv**2
        # Set up matrix for easy indexing (see vismat below)
        maskmat = numpy.zeros([nant, nant], dtype="bool")
        maskmat[stn1, stn2] = mask
        maskmat[stn2, stn1] = mask
        triple_mask *= (
            mask[:, numpy.newaxis] * maskmat[stn2, :] * maskmat[stn1, :]
        ).reshape(-1)

    return triple_mask


def _generate_closure_comparison_plot_colours(
    uvw: npt.NDArray[numpy.float_],
    stn1: npt.NDArray[numpy.int_],
    stn2: npt.NDArray[numpy.int_],
    nant: int,
    ctype: Optional[str] = None,
) -> Union[npt.NDArray[numpy.float_], None]:
    """Calculate a variable to use for colouring scatter plots.

    Helper function to generate colours for scatter plots.

    :param uvw: 2D array of uvw coordinates [nvis, 3].
    :param stn1: 1D array of baseline antenna indices (first antenna) [nvis].
    :param stn2: 1D array of baseline antenna indices (second antenna) [nvis].
    :param nant: number of antennas included in correlation matrix.
    :param ctype: What to use for colouring. Allowed options are "uvmin", which will
        set colours based on the minimum baseline length in each triplet, "uvmax",
        which will set colours based on the maximum baseline length in each triplet,
        and "uvdiff", which will set colours based on uvmax - uvmin.
        Default is none, which will also return None.
    :return: 1D array of floats to use for plot colours [len(visdata)] or None.
    """
    colour = None
    if ctype is not None:
        if ctype not in ("uvmin", "uvmax", "uvdiff"):
            raise ValueError(f"Unknown parameter ctype: {ctype}")
        bl = numpy.sqrt(uvw[:, 0] ** 2 + uvw[:, 1] ** 2)
        blmat = numpy.zeros([nant, nant])
        blmat[stn1, stn2] = bl
        blmat[stn2, stn1] = bl
        bl1len = (bl[:, numpy.newaxis] * numpy.ones((1, nant))).reshape(-1)
        if ctype == "uvmin":
            bl23min = (
                numpy.ones((len(bl), 1))
                * numpy.min(numpy.stack([blmat[stn2, :], blmat[stn1, :]]), axis=0)
            ).reshape(-1)
            colour = numpy.min(numpy.stack([bl1len, bl23min]), axis=0)
        elif ctype == "uvmax":
            bl23max = (
                numpy.ones((len(bl), 1))
                * numpy.max(numpy.stack([blmat[stn2, :], blmat[stn1, :]]), axis=0)
            ).reshape(-1)
            colour = numpy.max(numpy.stack([bl1len, bl23max]), axis=0)
        elif ctype == "uvdiff":
            bl23max = (
                numpy.ones((len(bl), 1))
                * numpy.max(numpy.stack([blmat[stn2, :], blmat[stn1, :]]), axis=0)
            ).reshape(-1)
            bl23min = (
                numpy.ones((len(bl), 1))
                * numpy.min(numpy.stack([blmat[stn2, :], blmat[stn1, :]]), axis=0)
            ).reshape(-1)
            colour = numpy.max(numpy.stack([bl1len, bl23max]), axis=0) - numpy.min(
                numpy.stack([bl1len, bl23min]), axis=0
            )

    return colour


# pylint: disable=too-many-locals,too-many-statements
def _update_closure_comparison_plot_rows(
    axs: npt.NDArray[Any],  # matplotlib.axes.Axes
    ax_id: int,
    pol: str,
    visdata: npt.NDArray[numpy.complex_],
    mdldata: npt.NDArray[numpy.complex_],
    bl_ids: npt.NDArray[numpy.int_],
    triple_mask: npt.NDArray[numpy.bool_],
    colour: Union[npt.NDArray[numpy.float_], None],
    ctype: Optional[str] = None,
) -> None:
    """Update polarisation panels for generate_closure_comparison_plots.

    Helper function to deal with graphics for one polarisation.

    :param axs: numpy array of matplotlib Axes objects. There is one row of panels per
        polarisation and three columns of panels (amplitude, phase and phase histogram).
    :param ax_id: axis indexing parameter for this polarisation.
    :param pol: polarisation label.
    :param visdata: 1D array of visibilities.
    :param mdldata: 1D array of model visibilities [len(visdata)].
    :param bl_ids: 2D array of baseline antenna indices [2, len(visdata)].
    :param triple_mask: 1D mask of unique closure triplets [len(visdata) * nant]
    :param colour: 1D array of floats to use for plot colours [len(visdata)] or None.
    """
    triple_prod_vis, triple_prod_mdl = _generate_triple_products(
        visdata, mdldata, bl_ids
    )

    # Apply masks and remove flagged data
    triple_mask = triple_mask.copy() * (triple_prod_vis != 0)
    triple_prod_vis = triple_prod_vis[triple_mask]
    triple_prod_mdl = triple_prod_mdl[triple_mask]
    if isinstance(colour, numpy.ndarray):
        colour = colour.copy()[triple_mask]

    ax = axs[ax_id, 0]
    _scatterplot(
        ax=ax,
        x=numpy.abs(triple_prod_mdl),
        y=numpy.abs(triple_prod_vis),
        c=colour,
    )
    ax.locator_params(nbins=5)
    ax.set_xlabel("model amplitude")
    ax.set_ylabel("vis amplitude")
    ax.set_title(f"{pol} closure amplitude vs model")
    ax.set_aspect("equal")
    ax.grid()

    ax = axs[ax_id, 1]
    vis_angle = numpy.angle(triple_prod_vis, deg=True)
    angle_diff = vis_angle - numpy.angle(triple_prod_mdl, deg=True)
    vis_angle += 360 * (angle_diff < -180)
    vis_angle -= 360 * (angle_diff > +180)
    angle_diff += 360 * (angle_diff < -180)
    angle_diff -= 360 * (angle_diff > +180)
    _scatterplot(ax=ax, x=numpy.angle(triple_prod_mdl, deg=True), y=vis_angle, c=colour)
    ax.set_xlabel("model phase (deg)")
    ax.set_ylabel("vis phase (deg)")
    ax.set_title(f"{pol} closure phase vs model")
    ax.set_xlim([-180, 180])
    ax.set_ylim([-180, 180])
    ax.set_aspect("equal")
    ax.grid()

    ax = axs[ax_id, 2]
    ax.set_xlabel("Phase bin (degrees)")
    ax.set_ylabel("Count")
    ax.set_title(f"{pol} phase difference histogram")
    ax.hist(
        angle_diff,
        bins=range(-180, 181, 3),
        histtype="step",
        density=True,
        color="b",
        label=rf"std: {numpy.std(angle_diff):.1f}$^\circ$",
    )
    ax.grid()
    ax.legend(loc=1)

    if isinstance(colour, numpy.ndarray):
        mappable = cm.ScalarMappable(cmap="jet")
        mappable.set_array([numpy.min(colour), numpy.max(colour)])
        divider = make_axes_locatable(ax)
        cca = divider.append_axes("bottom", size="7%", pad=0.6)
        # colorbar(mappable, cax=cca, ticks=[c1,0.,c2], orientation="horizontal")
        if ctype == "uvmin":
            label = "Minimum baseline in triple (metres)"
        elif ctype == "uvmax":
            label = "Maximum baseline in triple (metres)"
        elif ctype == "uvdiff":
            label = "Max - min baseline in triple (metres)"
        else:
            label = ""
        plt.colorbar(mappable, cax=cca, orientation="horizontal", label=label)


def _generate_triple_products(
    visdata: npt.NDArray[numpy.complex_],
    mdldata: npt.NDArray[numpy.complex_],
    bl_ids: npt.NDArray[numpy.int_],
) -> tuple[npt.NDArray[numpy.complex_], npt.NDArray[numpy.complex_]]:
    """Generate visibility and model triple products.

    Helper function to generate closure triplets. For each baseline, triplets are
    generated by selecting all columns for the two relevant rows of a correlation
    matrix.

    Using the correlation matrix speeds up the cross matching, but leads to duplicate
    triplets. These can be masked out using  _generate_closure_comparison_plot_mask.

    :param visdata: 1D array of visibilities.
    :param mdldata: 1D array of model visibilities.
    :param bl_ids: 2D array of baseline antenna indices [2, len(visdata)].
    :return: 1D array of visdata triple products, 1D array of mdldata triple products.
    """
    nant = numpy.max(bl_ids) + 1
    stn1 = bl_ids[0]
    stn2 = bl_ids[1]

    # Set up correlation matrices for easy indexing
    vismat = numpy.zeros([nant, nant], dtype="complex")
    mdlmat = numpy.zeros([nant, nant], dtype="complex")
    vismat[stn1, stn2] = visdata
    mdlmat[stn1, stn2] = mdldata
    vismat[stn2, stn1] = visdata.conj()
    mdlmat[stn2, stn1] = mdldata.conj()

    triple_prod_vis = (
        visdata[:, numpy.newaxis] * vismat[stn2, :] * vismat[stn1, :].conj()
    ).reshape(-1)
    triple_prod_mdl = (
        mdldata[:, numpy.newaxis] * mdlmat[stn2, :] * mdlmat[stn1, :].conj()
    ).reshape(-1)

    return triple_prod_vis, triple_prod_mdl


# mypy: disable-error-code="attr-defined"
def _scatterplot(
    ax: matplotlib.axes.Axes,
    x: npt.NDArray[numpy.float_],
    y: npt.NDArray[numpy.float_],
    c: Union[npt.NDArray[numpy.float_], None],
) -> None:
    """Do scatter plot. Do it in stages if there are colours.

    Scatter plots are very slow if there are many different coloured points. It is
    faster if it can throw down the same marker for all points. So it can be more
    efficient to break a coloured scatter plot into a series of monochrome plots.
    """
    cmap = cm.jet  # pylint: disable=no-member

    if isinstance(c, numpy.ndarray):
        ncol = 8
        bins = numpy.linspace(numpy.max(c), numpy.min(c), num=ncol)[::-1]
        inds = numpy.digitize(c, bins=bins, right=True)
        for cval in range(ncol):
            mask = inds == cval
            ax.scatter(
                x=x[mask],
                y=y[mask],
                color=cmap(cval / ncol),
                s=1,
                marker="*",
            )
    else:
        ax.scatter(x=x, y=y, c=c, s=1, marker="*")
