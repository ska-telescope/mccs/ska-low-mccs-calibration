"""Sky Model Support."""

__all__ = [
    "gsmodel",
    "gsmap_lsm",
    "solar_lsm",
    "predict_vis",
    "generate_ft_phasor",
    "dft_sky_to_vis",
]

import logging
from typing import Union

import healpy as hp
import numpy
import numpy.typing as npt
import scipy.constants
from astropy import units
from astropy.coordinates import AltAz, SkyCoord, get_sun
from pygdsm import GlobalSkyModel16

from ska_low_mccs_calibration.eep import normalise_eeps, resample_eeps

log = logging.getLogger("mccs-calibration-sky-model-logger")


def gsmodel(
    frequency_mhz: npt.ArrayLike, nside: int, include_cmb: bool = True
) -> npt.NDArray[Union[numpy.float32, numpy.float64]]:
    """Call PyGDSM to evaluate the diffuse galactic sky model.

    PyGDSM model GlobalSkyModel16 is used.

    :param frequency_mhz: Frequency or list of frequencies to evaluate (MHz).
    :param nside: Desired Healpix nside parameter
    :param include_cmb: Whether to include CMB in generated sky model.
    :return: List containing the Healpix map at each frequency
    """
    # Wrap it in an array in case it has a strange ArrayLike type
    frequency_mhz = numpy.array(frequency_mhz)
    if numpy.ndim(frequency_mhz) == 0:
        frequency_mhz = [frequency_mhz]

    nside_out = nside
    gsm_2016 = GlobalSkyModel16(
        freq_unit="MHz",
        include_cmb=include_cmb,
        # interpolation="PCHIP",
    )

    gsmap = []
    for freq in frequency_mhz:
        cgsmap = gsm_2016.generate(freq)
        cgsmap = hp.pixelfunc.ud_grade(
            cgsmap,
            nside_out,
            pess=False,
            order_in="RING",
            order_out="RING",
            power=None,
            dtype=None,
        )
        gsmap.append(cgsmap)

    return numpy.array(gsmap)


def gsmap_lsm(
    gsmap: npt.ArrayLike,
    location: units.Quantity,
    time: units.Quantity,
    elevation_min: float = 0.0,
) -> list[npt.NDArray[Union[numpy.float32, numpy.float64]]]:
    """Extract relevant pixels from the GSM.

    Return the azimuth, elevation and temperature values of all pixels in
    the Healpix map above elevation_min degrees.

    :param ~numpy.ndarray, list gsmap: Healpix map.
    :param location: Astropy coordinates of the observation location.
    :param time: Astropy time of the observation.
    :param elevation_min: min elevation cutoff in degrees (default is 0 deg)
    :return: [azimuth_deg, elevation_deg, temperature]
    """
    # Wrap it in an array in case it has a strange ArrayLike type
    gsmap = numpy.array(gsmap)
    nside = hp.pixelfunc.get_nside(gsmap)
    npix = hp.pixelfunc.get_map_size(gsmap)

    ipix = numpy.arange(npix, dtype=numpy.uint32)
    c_coord = hp.pixelfunc.pix2ang(nside, ipix, nest=False, lonlat=True)
    skypos = SkyCoord(l=c_coord[0], b=c_coord[1], frame="galactic", unit="deg")
    skypos_altaz = skypos.transform_to(AltAz(obstime=time, location=location))
    azimuth_deg = numpy.array(skypos_altaz.az.degree)
    elevation_deg = numpy.array(skypos_altaz.alt.degree)

    valid = elevation_deg > elevation_min

    return [azimuth_deg[valid], elevation_deg[valid], gsmap[valid]]


def solar_lsm(
    frequency_mhz: npt.ArrayLike,
    location: units.Quantity,
    time: units.Quantity,
) -> list[npt.NDArray[Union[numpy.float32, numpy.float64]]]:
    """Get the position of the sun and estimate the temperature.

    Temperature is based on the analytical expression for the quiet Sun in the
    range 30 - 350 MHz from Benz A. O. (2009) "Radio emission of the quiet sun."
    Landolt Börnstein, 4B (103). This will be an underestimate when the sun is active.

    :param ~numpy.ndarray, list, float frequency_mhz: Frequency or list of frequencies
        to evaluate (MHz).
    :param location: Astropy coordinates of the observation location.
    :param time: Astropy time of the observation.
    :return: [azimuth_deg, elevation_deg, temperature]
    """
    # Wrap it in an array in case it has a strange ArrayLike type
    frequency_mhz = numpy.array(frequency_mhz)

    speed_of_light = scipy.constants.c
    k_b = scipy.constants.k

    cnst1 = 2.0 * k_b * numpy.power(frequency_mhz * 1e6, 2) / (speed_of_light**2)
    temperature = 1.94e-4 * numpy.power(frequency_mhz, 1.992) * 1e-22 / cnst1

    skypos = get_sun(time)
    skypos_altaz = skypos.transform_to(AltAz(obstime=time, location=location))
    azimuth_deg = skypos_altaz.az.degree
    elevation_deg = skypos_altaz.alt.degree

    return [azimuth_deg, elevation_deg, temperature]


def predict_vis(  # pylint: disable=too-many-arguments
    eeps: Union[None, list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]],
    eep_rotation_deg: float,
    index_array: npt.ArrayLike,
    delay_ns: npt.ArrayLike,
    frequency_mhz: float,
    azimuth_deg: npt.ArrayLike,
    elevation_deg: npt.ArrayLike,
    lsm: npt.ArrayLike,
    normalise_beam: bool = False,
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """Evaluate the DFT of the sky model for each baseline.

    :param eeps: list of four EEP sets: [Xphi, Xtheta, Yphi, Ytheta]. Set to None to
        ignore beams in prediction.
    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees.
        Not used if eeps = None.
    :param ~numpy.ndarray, list index_array: first and second antenna indices for each
        baseline
    :param ~numpy.ndarray, list delay_ns: array of shape (3,nbaselines) containing the
        relative baseline delays in nanoseconds
    :param frequency_mhz: observing frequency in MHz
    :param ~numpy.ndarray, list azimuth_deg: array of shape (nskypixels) containing
        pixel azimuths.
    :param ~numpy.ndarray, list elevation_deg: array of shape (nskypixels) containing
        zenith angles.
    :param ~numpy.ndarray, list lsm: list of pixel values
    :param normalise_beam: if true, normalise EEPs by the power integral over the set
        of pixels. Default is false.
    :return: complex model visibilities [XX, XY, YX, YY]
    """
    # Ensure that these are numpy arrays
    index_array = numpy.array(index_array)
    delay_ns = numpy.array(delay_ns)
    azimuth_deg = numpy.array(azimuth_deg)
    elevation_deg = numpy.array(elevation_deg)
    lsm = numpy.array(lsm)

    # Generate phase shifts for the DFT from sky to vis (nbaseline, nskypixels)
    log.info("predict_vis: Generating phase shifts for DFT from sky to vis")
    ft_arg_complex = generate_ft_phasor(
        delay_ns, frequency_mhz, azimuth_deg, elevation_deg
    )

    # Resample beam models at the LSM pixel centres
    if eeps is not None:
        log.info("predict_vis: Resampling beam models at LSM pixel centres")
        lsm_eeps = resample_eeps(eeps, eep_rotation_deg, azimuth_deg, elevation_deg)
        if normalise_beam:
            lsm_eeps = normalise_eeps(lsm_eeps)

    else:
        lsm_eeps = None

    # Evaluate the DFT of the sky model for each baseline
    log.info("predict_vis: Evaluate DFT from sky to vis")
    model_products = dft_sky_to_vis(lsm_eeps, index_array, ft_arg_complex, lsm)

    return model_products


def generate_ft_phasor(
    delay_ns: npt.ArrayLike,
    frequency_mhz: float,
    azimuth_deg: npt.ArrayLike,
    elevation_deg: npt.ArrayLike,
) -> npt.NDArray[Union[numpy.complex64, numpy.complex128]]:
    """Generate Fourier phase shifts for the LSM.

    Generate a complex array of phase shifts for the DFT from sky model pixels to sky
    model visibilities.

    In keeping with standard references and real-to-complex Fourier transforms from
    standard packages like FFTW and CUFFT, this is a forward transform and the sign of
    the Fourier exponent is negative.

    :param ~numpy.ndarray, list delay_ns: array of shape (3,nbaselines) containing the
        relative baseline delays in nanoseconds.
    :param frequency_mhz: observing frequency in MHz
    :param ~numpy.ndarray, list azimuth_deg: array of shape (nskypixels) containing
        pixel azimuths.
    :param ~numpy.ndarray, list elevation_deg: array of shape (nskypixels) containing
        zenith angles.
    :return: complex array of DFT phase terms, shape (nbaselines,nskypixels).
    """
    # Ensure that these are numpy arrays
    delay_ns = numpy.array(delay_ns)
    azimuth_deg = numpy.array(azimuth_deg)
    elevation_deg = numpy.array(elevation_deg)

    azimuth = azimuth_deg * numpy.pi / 180.0
    elevation = elevation_deg * numpy.pi / 180.0

    # l, m, n at pixel centre
    l_pix = numpy.cos(elevation) * numpy.sin(azimuth)
    m_pix = numpy.cos(elevation) * numpy.cos(azimuth)
    n_pix = numpy.sin(elevation)

    # phase terms in the u, v, w directions (-2*pi*f*t)
    scale = -2j * numpy.pi * 1e-09 * frequency_mhz * 1e6
    u_scl = delay_ns[0] * scale
    v_scl = delay_ns[1] * scale
    w_scl = delay_ns[2] * scale

    # Fourier phasor with shape (nbaseline, nskypixels)
    return numpy.exp(
        numpy.outer(u_scl, l_pix)
        + numpy.outer(v_scl, m_pix)
        + numpy.outer(w_scl, n_pix)
    )


def dft_sky_to_vis(
    eeps: Union[None, list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]],
    index_array: npt.ArrayLike,
    ft_phasor: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    lsm: npt.ArrayLike,
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """Evaluate the DFT of the sky model for each baseline.

    Evaluation can be done without EEPs, with a single set of EEPs for all antennas,
    or with a separate set of EEPs for each antenna.

    :param eeps: list of four EEP sets: [Xphi, Xtheta, Yphi, Ytheta]. Set to None to
        ignore beams in the DFT. The first dimension of each array is the antenna index,
        which has a length of one when a single beam model is used for all antennas.
    :param ~numpy.ndarray, list index_array: first and second antenna indices for each
        baseline
    :param ~numpy.ndarray, list ft_phasor: array of DFT phase terms between pixels and
        baselines
    :param ~numpy.ndarray, list lsm: list of pixel values
    :return: complex model visibilities [XX, XY, YX, YY]
    """
    # Ensure that these are numpy arrays
    index_array = numpy.array(index_array)
    lsm = numpy.array(lsm)

    if eeps is None:
        # Prediction without a beam: simple phase up
        xx_product = yy_product = ft_phasor @ lsm
        xy_product = yx_product = numpy.zeros(xx_product.shape, xx_product.dtype)

    elif eeps[0].shape[0] == 1:
        # Prediction with a single beam: multiply beam and sky
        eep_x_phi = eeps[0][0]
        eep_x_theta = eeps[1][0]
        eep_y_phi = eeps[2][0]
        eep_y_theta = eeps[3][0]
        xx_product = yy_product = ft_phasor @ lsm
        xy_product = yx_product = numpy.zeros(xx_product.shape, xx_product.dtype)

        xx_product = ft_phasor @ (
            (eep_x_phi * eep_x_phi.conj() + eep_x_theta * eep_x_theta.conj()) * lsm
        )
        xy_product = ft_phasor @ (
            (eep_x_phi * eep_y_phi.conj() + eep_x_theta * eep_y_theta.conj()) * lsm
        )
        yx_product = ft_phasor @ (
            (eep_y_phi * eep_x_phi.conj() + eep_y_theta * eep_x_theta.conj()) * lsm
        )
        yy_product = ft_phasor @ (
            (eep_y_phi * eep_y_phi.conj() + eep_y_theta * eep_y_theta.conj()) * lsm
        )

    else:
        # Prediction with separate antenna beams: multiply beam per baseline and pixel
        eep_x_phi = eeps[0]
        eep_x_theta = eeps[1]
        eep_y_phi = eeps[2]
        eep_y_theta = eeps[3]

        # It is faster to pre-calc eep_x_phi[p_ant1], etc, but they use more memory
        # A loop over EEPs is a bit faster than direct numpy.
        # Perhaps because it uses less memory?
        xx_product = (
            numpy.array(
                [
                    eep_x_phi[i1] * eep_x_phi[i2].conj()
                    + eep_x_theta[i1] * eep_x_theta[i2].conj()
                    for (i1, i2) in index_array
                ]
            )
            * ft_phasor
        ) @ lsm
        xy_product = (
            numpy.array(
                [
                    eep_x_phi[i1] * eep_y_phi[i2].conj()
                    + eep_x_theta[i1] * eep_y_theta[i2].conj()
                    for (i1, i2) in index_array
                ]
            )
            * ft_phasor
        ) @ lsm
        yx_product = (
            numpy.array(
                [
                    eep_y_phi[i1] * eep_x_phi[i2].conj()
                    + eep_y_theta[i1] * eep_x_theta[i2].conj()
                    for (i1, i2) in index_array
                ]
            )
            * ft_phasor
        ) @ lsm
        yy_product = (
            numpy.array(
                [
                    eep_y_phi[i1] * eep_y_phi[i2].conj()
                    + eep_y_theta[i1] * eep_y_theta[i2].conj()
                    for (i1, i2) in index_array
                ]
            )
            * ft_phasor
        ) @ lsm

    return [xx_product, xy_product, yx_product, yy_product]
