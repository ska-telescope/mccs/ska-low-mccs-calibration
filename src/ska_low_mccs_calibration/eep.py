"""Embedded Element Pattern Support."""

# mypy is complaining about the station_beam_matrix return. Why? Disabling.
# mypy: disable-error-code="no-any-return"

__all__ = [
    "convert_eep2npy",
    "load_eeps",
    "get_nearest_file",
    "azel_to_eep_indices",
    "reconfigure_eeps",
    "resample_eeps",
    "normalise_eeps",
    "station_rotation_matrix",
    "station_beam_matrix",
    "eep_pa_rotation",
]

import glob
import logging
import os
import re
import warnings
from typing import Union

import numpy
import numpy.typing as npt
import scipy
from astropy import units
from astropy.coordinates import AltAz, HADec, SkyCoord

log = logging.getLogger("mccs-calibration-eep-logger")


def convert_eep2npy(
    eep_filename: str, npy_dir: str = ".", dtype: npt.DTypeLike = numpy.complex64
) -> None:
    """Convert MATLAB EEP files to NumPy format and write them to disk.

    EEPs are split into separate files for the phi and theta polarisations.

    :param eep_filename: MATLAB filename or list of names to load.
    :param npy_dir: Output directory for npy files. Default is the current
        working directory.
    :param ~numpy.dtype dtype: The NumPy data-type to use for the output arrays. MATLAB
        files have dtype=complex128, but by default they will be reduced
        on output to dtype=complex64.
    """
    if npy_dir is None or npy_dir == "":
        npy_dir = "."

    # make a list of one or more files corresponding to eep_filename
    file_list = glob.glob(eep_filename)
    if len(file_list) == 0:
        log.warning("convert_eep2npy: eep_filename %s not found", eep_filename)

    # Loop over input MATLAB files and do conversion
    for name in file_list:
        # Get the current filename and strip off .mat if it is there
        basename = os.path.basename(name)
        splitext = os.path.splitext(basename)
        if splitext[1].lower() == ".mat":
            basename = splitext[0]

        # Generate the two output filenames and add the directory
        outname_phi = npy_dir + "/" + basename + "_phi.npy"
        outname_theta = npy_dir + "/" + basename + "_theta.npy"

        # Could test separately, but safer if the user cleans up
        if os.path.exists(outname_phi) or os.path.exists(outname_theta):
            log.warning("convert_eep2npy: Output files exist. Skipping.")
            log.warning("convert_eep2npy:  - phi: %s", outname_phi)
            log.warning("convert_eep2npy:  - theta: %s", outname_theta)
            continue

        log.info(
            "convert_eep2npy: Extracting arrays from %s "
            + "and saving as %s NumPy arrays",
            name,
            dtype,
        )

        # Extract the MATLAB data into NumPy ndarrays
        data21 = scipy.io.loadmat(name)
        mathlab_variables = scipy.io.whosmat(name)
        mathlab_variables = numpy.array(mathlab_variables, dtype=object)
        mathlab_variables = numpy.transpose(mathlab_variables)
        var_name = mathlab_variables[0]

        # Convert to required precision
        beam_pattern_phi = data21[var_name[0]].astype(dtype)
        beam_pattern_theta = data21[var_name[1]].astype(dtype)

        # Convert to required precision
        if beam_pattern_phi.dtype != dtype:
            beam_pattern_phi = beam_pattern_phi.astype(dtype)
            beam_pattern_theta = beam_pattern_theta.astype(dtype)

        numpy.save(outname_phi, beam_pattern_phi)
        numpy.save(outname_theta, beam_pattern_theta)


def load_eeps_npy(
    frequency_str: str,
    filebase: str,
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """
    Load EEP image files in NumPy .npy format.

    EEPs are split into separate files for the phi and theta polarisations.

    :param frequency_str: A string containing the frequency of the file
        to be accessed.
    :param filebase: The path prepended to the start of filenames, before
        frequency specification. Files are assumed to have a specific form,
        which for polarisation combination X/phi is: filebase<freq>MHz_Xpol_phi.npy.
    :return: [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]. One ndarray
        for each of the four polarisation combinations. Dimensions are
        [azimuth, zenith angle, antenna (in data order)].
    """
    name_x_phi = f"{filebase}{frequency_str}MHz_Xpol_phi.npy"
    name_x_theta = f"{filebase}{frequency_str}MHz_Xpol_theta.npy"
    name_y_phi = f"{filebase}{frequency_str}MHz_Ypol_phi.npy"
    name_y_theta = f"{filebase}{frequency_str}MHz_Ypol_theta.npy"

    # Check that the files exist
    for name in [name_x_phi, name_x_theta, name_y_phi, name_y_theta]:
        if not os.path.isfile(name):
            raise ValueError(f"load_eeps: cannot access file '{name}'")

    # Load the files
    eep_x_phi = numpy.load(name_x_phi)
    eep_x_theta = numpy.load(name_x_theta)
    eep_y_phi = numpy.load(name_y_phi)
    eep_y_theta = numpy.load(name_y_theta)

    return [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]


def load_eeps_npz(
    frequency_str: str, filebase: str
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """
    Load EEP image files in NumPy .npz format.

    A single .npz file contains both the phi and theta polarisations.

    :param frequency_str: A string containing the frequency of the file
        to be accessed.
    :param filebase: The path prepended to the start of filenames, before
        frequency specification. Files are assumed to have a specific form,
        which for polarisation X is: filebase<freq>MHz_Xpol.npz.
    :return: [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]. One ndarray
        for each of the four polarisation combinations. Dimensions are
        [azimuth, zenith angle, antenna (in data order)].
    """
    name_x = f"{filebase}{frequency_str}MHz_Xpol.npz"
    name_y = f"{filebase}{frequency_str}MHz_Ypol.npz"

    # Check that the files exist
    if not os.path.isfile(name_x):
        raise ValueError(f"load_eeps: cannot access file '{name_x}'")
    if not os.path.isfile(name_y):
        raise ValueError(f"load_eeps: cannot access file '{name_y}'")

    # Load the files
    eep_x = numpy.load(name_x)
    eep_y = numpy.load(name_y)

    eep_x_phi = eep_x["Ephi"]
    eep_x_theta = eep_x["Etheta"]
    eep_y_phi = eep_y["Ephi"]
    eep_y_theta = eep_y["Etheta"]

    return [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]


def load_eeps(
    frequency_mhz: float,
    path: str,
    filebase: str = "FEKO_AAVS3_vogel_256_elem_50ohm_",
    dtype: npt.DTypeLike = numpy.complex64,
    conjugate: bool = True,
    suffix: str = ".npy",
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    # pylint: disable=too-many-arguments
    r"""Load EEP NumPy files into beam ndarrays of shape [az, za, nant].

    Antennas are reordered to match the visibility data order, as specified by
    antenna_order. EEPs are stored in separate arrays for the four polarisation
    combinations: X,Y instrument polarisations and phi,theta sky polarisations.

    Antenna-averaged EEPs can also be used, with shape = [az, za, 1] or [az, za].

    Note: if EEP images are in MATLAB format, then before calling this function
    one should first call convert_eep2npy() to convert to NumPy format.

    :param frequency_mhz: Frequency of data being processed, in MHz.
    :param path: Directory path to EEP image files
    :param filebase: Start of filenames, before frequency specification. Files
        are assumed to have a specific form, which depends on the suffix.
        Default is filebase="FEKO_AAVS3_vogel_256_elem_50ohm\_".
    :param antenna_order: The antenna order for these models in the visibility
        data. The first EEP image corresponds to antenna antenna_order[0] in
        the visibilities, for example. Default is antenna_order=None.
    :param ~numpy.dtype dtype: The NumPy data-type to use to store the arrays. MATLAB
        files have dtype=complex128, but by default convert_eep2npy() reduces
        the npy output to dtype=complex64. Default here is dtype=complex64.
    :param conjugate: Whether to conjugate the data. Default is True.
    :param suffix: The file extension to use. Default is ".npy". Different
        suffixes correspond to different file types which need to be handled
        differently.
    :return: [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]. One ndarray
        for each of the four polarisation combinations. Dimensions are
        [azimuth, zenith angle, antenna (in data order)].
    """
    filebase = f"{path}/{filebase}"

    # Replace frequency_mhz with the closest EEP_freq value
    if suffix == ".npy":
        file_glob = f"{filebase}*MHz_Xpol_phi{suffix}"
        loader = load_eeps_npy
    elif suffix == ".npz":
        file_glob = f"{filebase}*MHz_Xpol{suffix}"
        loader = load_eeps_npz
    else:
        raise ValueError(f"load_eeps: {suffix} is not a valid suffix")

    frequency_str = get_nearest_file(file_glob, frequency_mhz)
    log.debug("Selecting EEP files at %s MHz", frequency_str)
    [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta] = loader(frequency_str, filebase)

    if eep_x_phi.ndim == 2:
        # EEPs seem to have been averaged over antennas. Add len=1 dim for down stream
        eep_x_phi = eep_x_phi[..., numpy.newaxis]
        eep_x_theta = eep_x_theta[..., numpy.newaxis]
        eep_y_phi = eep_y_phi[..., numpy.newaxis]
        eep_y_theta = eep_y_theta[..., numpy.newaxis]
    elif eep_x_phi.ndim != 3:
        raise ValueError(f"EEPs should have 2 or 3 dimensions. Not {eep_x_phi.ndim}.")

    # Perform any conversions
    if eep_x_phi.dtype != dtype:
        log.info("Converting EEP arrays to type %s", numpy.dtype(dtype).name)
        eep_x_phi = eep_x_phi.astype(dtype)
        eep_x_theta = eep_x_theta.astype(dtype)
        eep_y_phi = eep_y_phi.astype(dtype)
        eep_y_theta = eep_y_theta.astype(dtype)
    if conjugate:
        log.debug("Complex conjugating EEP arrays")
        eep_x_phi = eep_x_phi.conj()
        eep_x_theta = eep_x_theta.conj()
        eep_y_phi = eep_y_phi.conj()
        eep_y_theta = eep_y_theta.conj()

    return [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]


def get_nearest_file(filename_pattern: str, frequency_mhz: float) -> str:
    """Find the closest frequency of the available EEP files.

    This method is very basic at this point and requires the filenames to
    contain the substring "<freq>MHz".

    :param filename_pattern: Filename pattern with standard glob wildcards to search
        for closest frequency match. Filenames must contain sub-string "<freq>MHz".
    :param frequency_mhz: Frequency of data being processed, in MHz.
    :returns: Frequency of the nearest file.
    """
    file_list = glob.glob(filename_pattern)
    if len(file_list) == 0:
        log.warning("convert_eep2npy: empty list of EEP filenames")

    freq_strings: list[str] = []
    for file in file_list:
        freq_str = re.search(r"\d+MHz", file)
        if freq_str is not None:
            freq_strings.append(str(freq_str.group(0)[:-3]))

    if len(freq_strings) == 0:
        raise FileNotFoundError(
            "get_nearest_file: no suitable files found containing substring "
            + f'"\\d+MHz" in search over {filename_pattern}'
        )

    freq_values = numpy.array(freq_strings, "float")

    idx = numpy.argmin(numpy.abs(freq_values - frequency_mhz))

    return str(freq_strings[idx])


def azel_to_eep_indices(
    eep_shape: npt.ArrayLike,
    eep_rotation_deg: float,
    azimuth_deg: npt.ArrayLike,
    elevation_deg: npt.ArrayLike,
) -> list[npt.NDArray[numpy.int_]]:
    """Return nearest EEP pixels for a set of azimuths and elevations.

    All elevations should be greater than or equal to 0. If the station is rotated
    in azimuth, the EEP pixel coordaintes are rotated accordingly.

    :param ~numpy.ndarray, list eep_shape: shape of EEP azimuth and zenith angle axes,
        [Naz,Nza]
    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees
    :param ~numpy.ndarray, list azimuth_deg: azimuth of pixels in degrees
    :param ~numpy.ndarray, list elevation_deg: elevation of pixels in degrees
    :return: [azimuth indices, zenith angle indices]
    """
    # Ensure that these are numpy arrays
    eep_shape = numpy.array(eep_shape)
    azimuth_deg = numpy.array(azimuth_deg)
    elevation_deg = numpy.array(elevation_deg)
    # Ensure that the elevation is in range
    if numpy.any(elevation_deg < 0) or numpy.any(elevation_deg > 90):
        raise ValueError("azel_to_eep_indices: elevations are out of range")
    zenith_angle_deg = 90.0 - elevation_deg

    # SDP and other packages such as astropy use the standard astronomical
    # definition of azimuth, which runs East from its zero point of North.
    # However the EEPs use the North-from-East definition, so there needs to
    # be a conversion.
    # Also, EEPs are generated assuming alignment with the cardinal directions.
    # If the station is rotated, rotate the aziumth axis here so that the correct
    # EEP pixels are selected (az_apparent = azimuth - az_station).
    azimuth_alt = numpy.mod(90.0 - azimuth_deg + eep_rotation_deg, 360.0)

    # Calculate the initial resolution of the EEPs
    #  - should perhaps get this from the MATLAB files...
    daz_deg = 360.0 / (eep_shape[0] - 1)
    dza_deg = 90.0 / (eep_shape[1] - 1)
    log.debug("azel_to_eep_indices: EEP res: [%f, %f] deg", daz_deg, dza_deg)

    # Find the nearest EEP pixel for each direction
    iaz = numpy.rint(azimuth_alt / daz_deg).astype(numpy.int_)
    iza = numpy.rint(zenith_angle_deg / dza_deg).astype(numpy.int_)

    return [iaz, iza]


def reconfigure_eeps(
    eeps: list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]],
    eep_rotation_deg: float = 0,
    azimuth_deg: npt.ArrayLike = numpy.array([], float),
    elevation_deg: npt.ArrayLike = numpy.array([], float),
    normalise: bool = True,
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """Reconfigure EEPs for operations with the local sky model. Deprecated.

    This amounts to a change of axis ordering and a normalisation with respect to pixel
    centres of the sky model. At present this is a simple nearest-pixel resampling.

    On input the shape of each EEP is [azimuth, zenith angle, antenna].
    On output the shape of each EEP is [antenna, zenith angle, azimuth].

    :param eeps: list of four EEPs: [Xphi, Xtheta, Yphi, Ytheta]
    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees.
        Only required if normalise = true.
    :param ~numpy.ndarray, list azimuth_deg: azimuth of new pixels in degrees.
        Only required if normalise = true.
    :param ~numpy.ndarray, list elevation_deg: elevation of new pixels in degrees.
        Only required if normalise = true.
    :param normalise: if true, normalise EEPs by the power integral over a set of
        pixels. Default is true.
    :return: updated beam arrays
    """
    warnings.warn(
        "reconfigure_eeps is deprecated. Use resample_eeps and normalise_eeps",
        DeprecationWarning,
    )
    eep_x_phi = eeps[0]
    eep_x_theta = eeps[1]
    eep_y_phi = eeps[2]
    eep_y_theta = eeps[3]

    shape = numpy.array(eep_x_phi.shape)
    if (
        numpy.any(eep_x_theta.shape != shape)
        or numpy.any(eep_y_phi.shape != shape)
        or numpy.any(eep_y_theta.shape != shape)
    ):
        raise ValueError("reconfigure_eeps: EEPs must have equal shapes")

    normalise_x = 1.0
    normalise_y = 1.0
    if normalise:
        # Ensure that these are numpy arrays
        azimuth_deg = numpy.array(azimuth_deg)
        elevation_deg = numpy.array(elevation_deg)

        if len(azimuth_deg) < 1 or len(elevation_deg) < 1:
            raise ValueError("reconfigure_eeps: require pixel list for normalisation")

        if numpy.any(elevation_deg < 0) or numpy.any(elevation_deg > 90):
            raise ValueError("reconfigure_eeps: elevations are out of range")

        # Get mapping from direction (az,el) to neareset EEP pixel
        [iaz, iza] = azel_to_eep_indices(
            shape[[0, 1]],
            eep_rotation_deg,
            azimuth_deg,
            elevation_deg,
        )

        # Resampling changes the beam solid angle integral, so need to normalise
        # for the beam state *after* resampling.

        normalise_x = 1.0 / numpy.sqrt(
            numpy.sum(numpy.abs(eep_x_phi[iaz, iza, :]) ** 2, axis=0)
            + numpy.sum(numpy.abs(eep_x_theta[iaz, iza, :]) ** 2, axis=0)
        ).reshape(-1, 1, 1)

        normalise_y = 1.0 / numpy.sqrt(
            numpy.sum(numpy.abs(eep_y_phi[iaz, iza, :]) ** 2, axis=0)
            + numpy.sum(numpy.abs(eep_y_theta[iaz, iza, :]) ** 2, axis=0)
        ).reshape(-1, 1, 1)

    # Reorder from [iaz, iza, antidx] to [antidx, iza, iaz] and normalise
    # - should antennas have the same normalisation?
    eep_x_phi = numpy.swapaxes(eep_x_phi, 0, 2) * normalise_x
    eep_x_theta = numpy.swapaxes(eep_x_theta, 0, 2) * normalise_x
    eep_y_phi = numpy.swapaxes(eep_y_phi, 0, 2) * normalise_y
    eep_y_theta = numpy.swapaxes(eep_y_theta, 0, 2) * normalise_y

    return [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]


def resample_eeps(
    eeps: list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]],
    eep_rotation_deg: float,
    azimuth_deg: npt.ArrayLike,
    elevation_deg: npt.ArrayLike,
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """Resample beam models at the LSM pixel centres.

    At present this is a simple nearest-pixel resampling.

    On input the shape of each EEP is [azimuth, zenith angle, antenna].
    On output the shape of each EEP is [antenna, pixel]

    :param eeps: list of four EEPs: [Xphi, Xtheta, Yphi, Ytheta]
    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees
    :param ~numpy.ndarray, list azimuth_deg: azimuth of new pixels in degrees
    :param ~numpy.ndarray, list elevation_deg: elevation of new pixels in degrees
    :return: updated beam arrays
    """
    # Ensure that these are numpy arrays
    azimuth_deg = numpy.array(azimuth_deg)
    elevation_deg = numpy.array(elevation_deg)

    eep_x_phi = eeps[0]
    eep_x_theta = eeps[1]
    eep_y_phi = eeps[2]
    eep_y_theta = eeps[3]

    shape = numpy.array(eep_x_phi.shape)
    if (
        numpy.any(eep_x_theta.shape != shape)
        or numpy.any(eep_y_phi.shape != shape)
        or numpy.any(eep_y_theta.shape != shape)
    ):
        raise ValueError("resample_eeps: EEPs must have equal shapes")

    if numpy.any(elevation_deg < 0) or numpy.any(elevation_deg > 90):
        raise ValueError("resample_eeps: elevations are out of range")

    # Get mapping from direction (az,el) to neareset EEP pixel
    [iaz, iza] = azel_to_eep_indices(
        shape[[0, 1]],
        eep_rotation_deg,
        azimuth_deg,
        elevation_deg,
    )

    return [
        eep_x_phi[iaz, iza, :].T,
        eep_x_theta[iaz, iza, :].T,
        eep_y_phi[iaz, iza, :].T,
        eep_y_theta[iaz, iza, :].T,
    ]


def normalise_eeps(
    eeps: list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]],
) -> list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]:
    """Normalise EEPs for operations with the local sky model.

    When calling resample_eeps for the all-sky diffuse sky model, re-sampling may
    change the beam solid angle integral. This function re-normalises the EEPs by the
    power integral over the re-sampled pixels.

    The shape of the each EEP should be [antenna, pixel].

    :param eeps: list of four EEPs: [Xphi, Xtheta, Yphi, Ytheta]
    :return: updated beam arrays
    """
    eep_x_phi = eeps[0]
    eep_x_theta = eeps[1]
    eep_y_phi = eeps[2]
    eep_y_theta = eeps[3]

    shape = numpy.array(eep_x_phi.shape)
    if (
        numpy.any(eep_x_theta.shape != shape)
        or numpy.any(eep_y_phi.shape != shape)
        or numpy.any(eep_y_theta.shape != shape)
    ):
        raise ValueError("normalise_eeps: EEPs must have equal shapes")
    if len(shape) != 2:
        raise ValueError("normalise_eeps: EEPs are expected to have two dimensions")
    if shape[1] < 1:
        raise ValueError("normalise_eeps: require at least one pixel for normalisation")

    normalise_x = 1.0 / numpy.sqrt(
        numpy.sum(numpy.abs(eep_x_phi) ** 2, axis=1, keepdims=True)
        + numpy.sum(numpy.abs(eep_x_theta) ** 2, axis=1, keepdims=True)
    )

    normalise_y = 1.0 / numpy.sqrt(
        numpy.sum(numpy.abs(eep_y_phi) ** 2, axis=1, keepdims=True)
        + numpy.sum(numpy.abs(eep_y_theta) ** 2, axis=1, keepdims=True)
    )

    eep_x_phi = eep_x_phi * normalise_x
    eep_x_theta = eep_x_theta * normalise_x
    eep_y_phi = eep_y_phi * normalise_y
    eep_y_theta = eep_y_theta * normalise_y

    return [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]


def station_rotation_matrix(
    eep_rotation_deg: float,
    reverse: bool = False,
) -> npt.NDArray[Union[numpy.complex64, numpy.complex128]]:
    """Return the rotation matrix for station polarisations relative to X=EW, Y=NS.

    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees
    :param reverse: whether to transpose the matrix for back-rotation. True by default.
    :return: polarisation rotation matrix
    """
    phi = eep_rotation_deg * numpy.pi / 180.0

    jones = numpy.array(
        [[numpy.cos(phi), -numpy.sin(phi)], [numpy.sin(phi), numpy.cos(phi)]]
    )

    if reverse:
        return jones.T

    return jones


def station_beam_matrix(
    eeps: list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]],
    eep_rotation_deg: float,
    location: units.Quantity,
    time: units.Quantity,
    right_ascension_deg: float,
    declination_deg: float,
    pa_correction: bool = False,
) -> npt.NDArray[Union[numpy.complex64, numpy.complex128]]:
    # pylint: disable=too-many-arguments
    """Return the station-averaged EEP Jones matrix in given direction.

    eeps is a list of four beam arrays with shape [azimuth, zenith angle, antenna].
    On output the sampled beam is averaged across stations into a [2, 2] numpy array.

    :param eeps: list of four EEP sets: [Xphi, Xtheta, Yphi, Ytheta]
    :param eep_rotation_deg: rotation of the station (and therefore EEPs) in degrees
    :param location: Astropy coordinates of the observation location.
    :param time: Astropy time of the observation.
    :param right_ascension_deg: right ascension of beam in degrees
    :param declination_deg: declination of beam in degrees
    :param pa_correction: whether to generate matrices that include the PA rotation
        from IAU coordinates aligned with declination and right ascension. False by
        default.
    :return: average beam Jones matrix
    """
    # Check Jones matrix elements
    shape = numpy.array(eeps[0].shape)
    if (
        numpy.any(eeps[1].shape != shape)
        or numpy.any(eeps[2].shape != shape)
        or numpy.any(eeps[3].shape != shape)
    ):
        raise ValueError("station_beam_matrix: EEPs must have equal shapes")

    # Get mapping from direction (az,el) to neareset EEP pixel
    skypos_radec = SkyCoord(right_ascension_deg, declination_deg, unit="deg")
    skypos_altaz = skypos_radec.transform_to(AltAz(obstime=time, location=location))
    if skypos_altaz.alt.degree < 0 or skypos_altaz.alt.degree > 90:
        raise ValueError("station_beam_matrix: elevation is out of range")
    [iaz, iza] = azel_to_eep_indices(
        shape[[0, 1]],
        eep_rotation_deg,
        skypos_altaz.az.degree,
        skypos_altaz.alt.degree,
    )

    # Pack EEPs into an array with shape [nstation, 2, 2]
    eep_beams = numpy.array(  # pylint: disable=too-many-function-args
        [
            eeps[0][iaz, iza, :],
            eeps[1][iaz, iza, :],
            eeps[2][iaz, iza, :],
            eeps[3][iaz, iza, :],
        ]
    ).T.reshape(-1, 2, 2)

    # Average over stations
    ave_beam = numpy.mean(eep_beams, axis=0)

    if pa_correction:
        ave_beam = eep_pa_rotation(
            ave_beam, location, time, right_ascension_deg, declination_deg
        )

    return ave_beam


def eep_pa_rotation(
    jones_array: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    location: units.Quantity,
    time: units.Quantity,
    right_ascension_deg: float,
    declination_deg: float,
) -> npt.NDArray[Union[numpy.complex64, numpy.complex128]]:
    """Apply a parallactic angle correction to a Jones matrix.

    eeps is a list of four beam arrays with shape [azimuth, zenith angle, antenna].
    On output the sampled beam is averaged across stations into a [2, 2] numpy array.

    :param jones_array: numpy array contain EEPs in Jones format with at least two
        dimensions: [..., 2, 2]
    :param location: Astropy coordinates of the observation location.
    :param time: Astropy time of the observation.
    :param right_ascension_deg: right ascension of beam in degrees
    :param declination_deg: declination of beam in degrees
    :param pa_correction: whether to generate matrices that include the PA rotation
        from IAU coordinates aligned with declination and right ascension. False by
        default.
    :return: average beam Jones matrix
    """
    if jones_array.ndim < 2:
        raise ValueError("eep_pa_rotation: rotation requires 2x2 Jones matrices")
    if jones_array.shape[-1] != 2 or jones_array.shape[-2] != 2:
        raise ValueError("eep_pa_rotation: rotation requires 2x2 Jones matrices")

    skypos_radec = SkyCoord(right_ascension_deg, declination_deg, unit="deg")
    skypos_hadec = skypos_radec.transform_to(HADec(obstime=time, location=location))

    lat = location.lat.radian
    ha = skypos_hadec.ha.radian
    dec = skypos_hadec.dec.radian

    PA = numpy.arctan2(
        numpy.cos(lat) * numpy.sin(ha),
        numpy.sin(lat) * numpy.cos(dec)
        - numpy.cos(lat) * numpy.sin(dec) * numpy.cos(ha),
    )
    # EEPs transform to [EW, NS] from [az, el]
    # PA transforms to [el, az] from [x, y]
    # Need to swap rows of PA before multiplying to match EEP ordering
    rot = numpy.array(
        [
            # original PA rotation matrix
            #   [numpy.cos(PA), -numpy.sin(PA)], [numpy.sin(PA), numpy.cos(PA)],
            # swap rows to change to [az, el] from [x, y]
            #   [numpy.sin(PA), numpy.cos(PA)], [numpy.cos(PA), -numpy.sin(PA)],
            # also change the sign of the y coefficients
            #  - standard left-handed issue with RA?
            #   [numpy.sin(PA), -numpy.cos(PA)], [numpy.cos(PA), numpy.sin(PA)],
            [numpy.sin(PA), -numpy.cos(PA)],
            [numpy.cos(PA), numpy.sin(PA)],
        ]
    )

    tmp = jones_array.copy()
    jones_array = numpy.einsum("...xp,py->...xy", tmp, rot)

    return jones_array
