"""Extra Utility Functions."""

# doesn't understand return type xarray.Visibility
# mypy: disable-error-code="no-any-return"

__all__ = [
    "enu_to_geocentric",
    "read_yaml_config",
    "read_station_config",
    "sdp_visibility_datamodel",
    "apply_gaintable",
]

import logging
from typing import Any, Union

import numpy
import numpy.typing as npt
import pandas
import xarray
import yaml
from astropy import units
from astropy.coordinates import AltAz, Angle, EarthLocation, SkyCoord
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.science_data_model import PolarisationFrame, ReceptorFrame
from ska_sdp_datamodels.visibility import Visibility

log = logging.getLogger("mccs-calibration-utils-logger")


def enu_to_geocentric(
    antpos_enu: npt.ArrayLike,
    location: units.Quantity,
) -> npt.NDArray[Union[numpy.float32, numpy.float64]]:
    """Convert East,North,Up antenna coordinates to geocentric.

    Convert antenna coordinates from the local frame to geocentric.

    :param ~numpy.ndarray, list antpos_enu: array of East,North,Up coordinates. This
        array must have shape [..., 3], with East,North,Up coordinates along the last
        axis.
    :param location: Astropy coordinates of the observation location.
    :return: ndarray with the same shape as antpos_enu and X,Y,Z coordinates
        along the last axis.
    """
    # Ensure that it is a numpy array
    antpos_enu = numpy.array(antpos_enu)

    # Ensure that it has appropriate dimensions
    if antpos_enu.ndim < 1:
        return numpy.array([])
    if antpos_enu.shape[-1] != 3:
        raise ValueError("enu_to_geocentric: last axis must have length 3")

    # First rotate ENU to local dx,dy,dz at local meridian
    #  - Rotate back to lat=0 where X = up, Y = east and Z = north
    lat = location.lat.to("rad").value
    xarr = antpos_enu[..., 2] * numpy.cos(-lat) + antpos_enu[..., 1] * numpy.sin(-lat)
    yarr = antpos_enu[..., 0]
    zarr = -antpos_enu[..., 2] * numpy.sin(-lat) + antpos_enu[..., 1] * numpy.cos(-lat)

    # Then rotate dx,dy,dz to dX,dY,dZ
    #  - Rotation about Z by -longitude
    lon = location.lon.to("rad").value
    dxgeo = xarr * numpy.cos(-lon) + yarr * numpy.sin(-lon)
    dygeo = -xarr * numpy.sin(-lon) + yarr * numpy.cos(-lon)
    dzgeo = zarr
    xgeo = location.to_geocentric()[0].value + dxgeo
    ygeo = location.to_geocentric()[1].value + dygeo
    zgeo = location.to_geocentric()[2].value + dzgeo

    return numpy.stack((xgeo, ygeo, zgeo), axis=-1)


def _finditem(yaml_data: dict[str, Any], target: str) -> Any:
    """Find the first instance of key in data and return the associated dict.

    :param yaml_data: dict to search
    :param target: target key to find
    :return: stations dictionary
    """
    if target in yaml_data:
        return yaml_data[target]
    for _, value in yaml_data.items():
        if isinstance(value, dict):
            item = _finditem(value, target)
            if item is not None:
                return item

    return None


def read_yaml_station(yaml_file: str) -> dict[str, Any]:
    """Read a YAML file and return station data.

    :param yaml_file: YAML file to read
    :return: dictionary contents for the current station
    """
    with open(yaml_file, "r", encoding="utf8") as file:
        yaml_data = yaml.safe_load(file)

    # The stations keyword can appear at different levels, e.g. when there are
    # additional levels for station clusters. So find the key rather than assuming a
    # location.
    stations = _finditem(yaml_data, "stations")
    if stations is None:
        raise ValueError("read_yaml_station: no stations in YAML")

    # Check that the YAML file contains a single station. If multiple stations are
    # combined in YAML, the appropriate name could be given to this function and
    if len(stations) != 1:
        raise ValueError("read_yaml_station: multiple stations in YAML")
    station_name = list(stations.keys())[0]

    log.info("read_yaml_station: extracting station %s", station_name)
    return stations[station_name]


def read_yaml_config(
    yaml_file: str,
) -> tuple[
    units.Quantity,
    npt.NDArray[numpy.bool_],
    tuple[npt.NDArray[numpy.int_], npt.NDArray[numpy.int_]],
    tuple[
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
    ],
    list[int],
    float,
]:
    """Extract configuration metadata from observation YAML file.

    :param yaml_file: YAML file to read
    :return: tuple of configuration parameters:
        location,  # Astropy array location
        antenna_masks,  # bool list of flagged antennas
        [vis ant1, vis ant 2],  # first and second antenna for each baseline
        [E, N, U],  # Local East, North, Up location of each antenna
        [XX, XY, YX, YY],  # index of each polarisation in vis dataset
        rotation,  # azimuthal rotation of station (bearing clockwise from north)
    """
    # Start by reading the metadata for the current station
    station_dict = read_yaml_station(yaml_file)

    return read_station_config(station_dict)


def read_station_config(station_dict: dict[str, Any]) -> tuple[
    units.Quantity,
    npt.NDArray[numpy.bool_],
    tuple[npt.NDArray[numpy.int_], npt.NDArray[numpy.int_]],
    tuple[
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
    ],
    list[int],
    float,
]:
    """
    Extract configuration metadata from a station_dict.

    :param station_dict: a dictionary containing config for a station
    :return: tuple of configuration parameters:
        location,  # Astropy array location
        antenna_masks,  # bool list of flagged antennas
        [vis ant1, vis ant 2],  # first and second antenna for each baseline
        [E, N, U],  # Local East, North, Up location of each antenna
        [XX, XY, YX, YY],  # index of each polarisation in vis dataset
        rotation,  # azimuthal rotation of station (bearing clockwise from north)
    """
    # Get station rotation information
    rotation = station_dict.get("rotation", 0.0)
    log.info("read_yaml_config: station rotation %f deg", rotation)

    # Get site reference information
    location = read_yaml_location(station_dict)

    # Get individual antenna information
    [antidx_yaml2data, antenna_masks, enu_array, pol_order] = read_yaml_antennas(
        station_dict
    )

    # Generate the row and column indices of a full visibility correlation matrix
    nant = len(antidx_yaml2data)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    # mask to exclude conjugate points
    uppert = row <= col

    # Set the first and second antenna in the correlation data
    # The order appears to be for upper triangular part of the correlation matrix, but
    # the visibilities come from the lower triangular part. Not sure why, but
    # generating indices for the upper triangular part then swapping the order. This
    # is equivalent to a conjugate transpose of each 2x2 coherency matrices.
    antidx_data2yaml = numpy.argsort(antidx_yaml2data).astype(antidx_yaml2data.dtype)
    p_ant2 = antidx_data2yaml[row[uppert]]
    p_ant1 = antidx_data2yaml[col[uppert]]

    return location, antenna_masks, (p_ant1, p_ant2), enu_array, pol_order, rotation


def read_yaml_location(station_dict: dict[str, Any]) -> units.Quantity:
    """Extract location data from observation yaml file.

    Get site latitude (deg), longitude (deg), elevation (m) from yaml file

    :param station_dict: YAML dictionary contents for the current station.
    :return: Astropy array location
    """
    # Get site latitude (deg), longitude (deg), elevation (m) from stations dict
    station_location = station_dict["reference"]

    return EarthLocation.from_geodetic(
        station_location["longitude"],
        station_location["latitude"],
        height=station_location["ellipsoidal_height"],
        ellipsoid=station_location["datum"],
    )


def read_yaml_antennas(
    station_dict: dict[str, Any],
) -> tuple[
    npt.NDArray[numpy.uint32],
    npt.NDArray[numpy.bool_],
    tuple[
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
        npt.NDArray[Union[numpy.float32, numpy.float64]],
    ],
    list[int],
]:
    """Extract antenna data from observation yaml file.

    :param station_dict: YAML dictionary contents for the current station.
    :return: tuple of configuration parameters:
        antidx_yaml2data,  # mapping from yaml antenna indices to vis indices
        antenna_masks,  # bool list of flagged antennas
        [E, N, U],  # Local East, North, Up location of each antenna
        [XX, XY, YX, YY],  # index of each polarisation in vis dataset
    """
    # Get antenna metadata dictionary
    antenna_dict = station_dict["antennas"]

    nant = len(antenna_dict)

    # Mapping from yaml file and EM models to antenna indices in the visibility dataset
    antidx_yaml2data = numpy.zeros(nant, dtype=numpy.uint32)

    # east, north, up are offsets from the reference location
    east = numpy.zeros(nant)
    north = numpy.zeros(nant)
    up = numpy.zeros(nant)

    # Store tpm_x and tpm_y for consistency checks
    tpm_x = numpy.zeros(nant, dtype=numpy.uint32)
    tpm_y = numpy.zeros(nant, dtype=numpy.uint32)

    # Flags for bad antennas
    antenna_masks = numpy.zeros(nant, dtype=bool)

    for antenna_config in antenna_dict.values():
        idx = int(antenna_config["eep"]) - 1
        tpm_id = station_dict["sps"]["tpms"][antenna_config["tpm"]]["id"]
        tpm_y[idx] = int(antenna_config["tpm_y_channel"])
        tpm_x[idx] = int(antenna_config["tpm_x_channel"])
        antidx_yaml2data[idx] = tpm_id * 16 + (tpm_y[idx] // 2)
        east[idx] = antenna_config["location_offset"]["east"]
        north[idx] = antenna_config["location_offset"]["north"]
        up[idx] = antenna_config["location_offset"]["up"]
        antenna_masks[idx] = antenna_config.get("masked", False)

    if numpy.min(antidx_yaml2data) < 0 or numpy.max(antidx_yaml2data) >= nant:
        raise ValueError(
            "read_yaml_antennas: Bad antenna index range in station dictionary: "
            + f"[{numpy.min(antidx_yaml2data)}, {numpy.max(antidx_yaml2data)}]"
        )

    # The X and Y polarisation order should be consistent (throw exception if not)
    #  - Otherwise, would need to return this information and reorder data
    # Data are labelled with Y,X ordering in the TPM, but this is wrong (see SPRTS-224)
    #  - For now assume all data have X,Y ordering, but add test once SPRTS-224 is done
    pol_order = [0, 1, 2, 3]
    # if numpy.all(tpm_y - tpm_x != 1):
    #     raise ValueError("read_yaml_antennas: unexpected TPM polarisation order")

    return antidx_yaml2data, antenna_masks, (east, north, up), pol_order


# pylint: disable=too-many-arguments
def check_datamodel_inputs(
    vis: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    flags: npt.NDArray[numpy.bool_],
    uvw: npt.NDArray[Union[numpy.float32, numpy.float64]],
    ant1: npt.NDArray[numpy.int_],
    ant2: npt.NDArray[numpy.int_],
    antpos_enu: npt.NDArray[Union[numpy.float32, numpy.float64]],
    time: units.Quantity,
) -> None:
    """Check inputs for sdp_visibility_datamodel.

    :param vis: 3D array of visibilties, shape [ntimes,nbaselines,npol]
    :param flags: 2D or 3D array of bad-data flags, shape [<ntimes>,nbaselines,npol]
    :param uvw: 2D or 3D array of antenna coordinates in metres, shape
        [ntimes,nbaselines,3]
    :param ~numpy.ndarray, list ant1: 1D array containing the indices of the first
        antenna in each baseline
    :param ~numpy.ndarray, list ant2: 1D array containing the indices of the second
        antenna in each baseline
    :param ~numpy.ndarray, list antpos_enu: Array of antenna East,North,Up coordinates
        in metres. This array must have shape [nant, 3], with East,North,Up coordinates
        along the 2nd axis.
    :param time: Astropy time of the observation time step.
    """
    if antpos_enu.ndim != 2:
        raise ValueError("sdp_visibility_datamodel: antpos_enu must have ndim = 2")
    if antpos_enu.shape[1] != 3:
        raise ValueError("sdp_visibility_datamodel: 2nd axis must have length 3")

    if vis.ndim != 3:
        raise ValueError(
            "sdp_visibility_datamodel: expect vis dim [ntime,nbaselines,npol]"
        )

    # Could use the same defaults for ant1 and ant2 as in create_visibility()

    if flags.shape != vis.shape:
        raise ValueError("sdp_visibility_datamodel: flags shape inconsistent with vis")
    if uvw.shape[:2] != vis.shape[:2]:
        raise ValueError("sdp_visibility_datamodel: uvw shape inconsistent with vis")
    if len(time) != vis.shape[0]:
        raise ValueError("sdp_visibility_datamodel: time length inconsistent with vis")
    if len(ant1) != vis.shape[1]:
        raise ValueError("sdp_visibility_datamodel: ant1 length inconsistent with vis")
    if len(ant2) != vis.shape[1]:
        raise ValueError("sdp_visibility_datamodel: ant2 length inconsistent with vis")


# pylint: disable=too-many-arguments
def sdp_visibility_datamodel(
    vis: npt.NDArray[Union[numpy.complex64, numpy.complex128]],
    flags: npt.NDArray[numpy.bool_],
    uvw: npt.NDArray[Union[numpy.float32, numpy.float64]],
    ant1: npt.ArrayLike,
    ant2: npt.ArrayLike,
    location: units.Quantity,
    antpos_enu: npt.ArrayLike,
    time: units.Quantity,
    int_time: float,
    frequency_mhz: float,
    channel_bandwidth_mhz: float = 0.78125,
) -> xarray.Dataset:
    """Package MCCS data in a ska-sdp-datamodels Visibility xarray.

    Currently set up for a single time step and frequency channel

    :param vis: 2D or 3D array of visibilties, shape [<ntimes>,nbaselines,npol]
    :param flags: 2D or 3D array of bad-data flags, shape [<ntimes>,nbaselines,npol]
    :param uvw: 2D or 3D array of antenna coordinates in metres, shape
        [<ntimes>,nbaselines,3]
    :param ~numpy.ndarray, list ant1: 1D array containing the indices of the first
        antenna in each baseline
    :param ~numpy.ndarray, list ant2: 1D array containing the indices of the second
        antenna in each baseline
    :param location: Astropy coordinates of the observation location.
    :param ~numpy.ndarray, list antpos_enu: Array of antenna East,North,Up coordinates
        in metres. This array must have shape [nant, 3], with East,North,Up coordinates
        along the 2nd axis.
    :param time: Astropy time of the observation time step.
    :param int_time: Integration time of data samples (seconds).
    :param frequency_mhz: Observing frequency of data channel (MHz).
    :param channel_bw_MHz: Channel bandwidth (MHz). Default = 400MHz/512 = 0.78125
    :return: ska-sdp-datamodels Visibility xarray containing visibilities and metadata
    """
    # Ensure that these are numpy arrays
    ant1 = numpy.array(ant1, dtype=numpy.int_)
    ant2 = numpy.array(ant2, dtype=numpy.int_)
    antpos_enu = numpy.array(antpos_enu)

    # Add single time dimension at the start if there isn't one
    if vis.ndim == 2:
        vis = vis[None, :, :]
        flags = flags[None, :, :]
        uvw = uvw[None, :, :]

    # Give time length of 1 if it is a scalar
    if time.shape == ():
        time = time.reshape(1)

    # Check inputs for consistent shapes
    check_datamodel_inputs(vis, flags, uvw, ant1, ant2, antpos_enu, time)

    weight = numpy.ones(vis.shape, dtype=vis.dtype.type(0).real.dtype)

    # Get coordinates of the phase centre.
    # As the uvw have not been precessed to ICRF, leave this in the altaz frame
    zen_aa = [
        AltAz(
            alt=Angle(90, unit="degree"),
            az=Angle(0, unit="degree"),
            obstime=time[len(time) // 2],
            location=location,
        )
    ]

    nant = antpos_enu.shape[0]

    vis_xarray = Visibility.constructor(
        configuration=Configuration().constructor(
            name="AAVS3",
            names=[f"ant{x:03d}" for x in range(1, nant + 1)],
            stations=[f"{x}" for x in range(1, nant + 1)],
            location=location,
            xyz=enu_to_geocentric(antpos_enu, location=location),
            receptor_frame=ReceptorFrame("linear"),
            diameter=1.0,  # Errors if not set
            mount="altaz",
        ),
        phasecentre=SkyCoord(zen_aa).altaz,
        time=time.mjd * 86400.0,
        integration_time=numpy.array([int_time] * len(time)),
        frequency=[frequency_mhz * 1e6],
        channel_bandwidth=[channel_bandwidth_mhz * 1e6],
        polarisation_frame=PolarisationFrame("linear"),
        source=f"Sky_at_mjd_{time.mjd}",
        meta=None,
        vis=vis[:, :, None, :],
        weight=weight[:, :, None, :],
        flags=flags[:, :, None, :],
        uvw=uvw,
        baselines=pandas.MultiIndex.from_tuples(
            [(ant1[bl], ant2[bl]) for bl in range(vis.shape[1])],
            names=("antenna1", "antenna2"),
        ),
    )

    return vis_xarray


def apply_gaintable(
    vis: xarray.Dataset,
    gt: xarray.Dataset,
    inverse: bool = False,
) -> xarray.Dataset:
    """Apply a GainTable to a Visibility.

    This is a temporary local version of ska-sdp-func-python.operations.apply_gaintable
    that avoid a bug in the original. Will remove once the ska-sdp-func-python version
    is fixed. The bug is that the function does not transpose the rightmost matrix.
    This may not always be the desired action, for instance if calibration was done
    separately for each polarised visibility type, but is required when the solutions
    are Jones matrices.

    Note: this is a temporary function and has not been made to robustly handle all
    situations. For instance, it ignores flags and does not properly handle sub-bands
    and partial solution intervals. It also assumes that the matrices being applied are
    Jones matrices and the general ska-sdp-datamodels xarrays are used.

    :param vis: ska-sdp-datamodels Visibility xarray containing visibilities.
    :param gt: ska-sdp-datamodels GainTable xarray containing gain solutions.
    :param inverse: Apply the inverse of the gain. This requires the gain matrices
        to be square. (default=False)
    :return: ska-sdp-datamodels Visibility xarray containing calibrated visibilities.
    """
    if vis.vis.ndim != gt.gain.ndim - 1:
        raise ValueError("apply_gaintable: incompatible shapes")
    if vis.vis.shape[-1] != gt.gain.shape[-1] * gt.gain.shape[-1]:
        raise ValueError("apply_gaintable: incompatible pol axis")
    if inverse and gt.gain.shape[-1] != gt.gain.shape[-2]:
        raise ValueError("apply_gaintable: gain inversion requires square matrices")

    shape = vis.vis.shape

    # inner pol dim for forward application, and also for the inverse since square
    npol = gt.gain.shape[-1]

    # need to know which dim has the antennas, so force the structure
    if vis.vis.ndim != 4 or vis.vis.shape[-1] != 4:
        raise ValueError("apply_gaintable: expecting ska-sdp-datamodels xarrays")

    if inverse:
        # use pinv rather than inv to catch singular values
        jones = numpy.linalg.pinv(gt.gain.data[..., :, :])
    else:
        jones = gt.gain.data

    vis.vis.data = numpy.einsum(
        "...pi,...ij,...qj->...pq",
        jones[:, vis.antenna1.data],
        vis.vis.data.reshape(shape[0], shape[1], shape[2], npol, npol),
        jones[:, vis.antenna2.data].conj(),
    ).reshape(shape)

    return vis
