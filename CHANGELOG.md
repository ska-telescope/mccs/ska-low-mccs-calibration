# Version History

1.0.1
-----
* [THORN-17] 1.0.0 release - all MCCS repos

0.4.0
-----
* [YAN-1923] Support for average EEPs

0.3.4
-----
* [YAN-1921] Added new qa_plots option for calibrate_mccs_visibility. Also colorbars for generate_closure_comparison_plots. Also added some support for 2D averaged EEPs. [MR26]
* [YAN-1837] Added new QA plotting functions. Also a new option for calibrate_mccs_visibility to control the low gain threshold and changed read_yaml_antennas to use the eep parameter when setting EEP indices. [MR25]
* [YAN-1856] Added option to ignore EEPs for faster sun-based calibration. [MR24]

0.3.3
-----
* [LOW-982] Correctly handle antenna `masked` key in station specification YAML files. [MR22]

0.3.2
-----
* [YAN-1796] - Added a sun-based sky model option. Also added a refant option for calibrate_mccs_visibility and enabled it for all values of jones_solve. [MR20]
* [MCCS-2211] - Removed Solver tango device. This device has moved to ska-low-mccs, leaving this repository free of TANGO. [MR21]

0.3.1
-----
* [MCCS-2209] - Add make use of station rotation config in solve. [MR18]

0.3.0
-----
* [YAN-1770] Changed to the order of antenna indices when loading HDF5 correlation data and replaced reconfigure_eeps with a lighter weight normalisation function. [MR16]

0.2.0
-----
* [MCCS-2125] Patched support for `back-rotation`, `nside`, `niter` and `conjugate` through to CalibrationSolver.Solve. [MR15]
* [YAN-1776] Polarisation back rotation options added to the calibration system. [MR14]
* [YAN-1735] Added support for rotated stations. A new rotation parameter is now returned by read_yaml_config and required by azel_to_eep_indices, reconfigure_eeps, eep_rotation_deg and predict_vis. It is controlled for all in calibrate_mccs_visibility using parameter eep_rotation_deg, which defaults to zero.
* [YAN-1735] Updated polarisation indices returned by read_yaml_config to include the transpose needed by the calibration scripts. This needs verification and may change again in the future. TPM Y channels are set as the first polarisations and X channels as the second.

0.1.0
-----
* [YAN-1597] Added the initial set of functions for calibrating Low stations
* [MCCS-1954] Bootstrap repo with ska-cookiecutter-pypackage
