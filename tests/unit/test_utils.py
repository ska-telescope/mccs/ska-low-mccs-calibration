#!/usr/bin/env python

"""Tests for `ska_low_mccs_calibration.utils`."""

import os

import numpy
from astropy.time import Time

from ska_low_mccs_calibration.utils import (
    enu_to_geocentric,
    read_yaml_antennas,
    read_yaml_config,
    read_yaml_location,
    read_yaml_station,
    sdp_visibility_datamodel,
)


def test_read_yaml_station() -> None:
    """Test read of station data."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    assert "id" in station_dict
    assert "reference" in station_dict
    assert "sps" in station_dict
    assert "pasd" in station_dict
    assert "antennas" in station_dict


def test_read_yaml_location() -> None:
    """Test read of station location data."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    location = read_yaml_location(station_dict)
    answer = (-2559377.99363595, 5095349.74793982, -2849124.06772244)
    for coord in range(3):
        assert abs(location.value[coord] - answer[coord]) < 1e-8


def test_read_yaml_antennas() -> None:
    """Test read of station antenna data."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    antidx_yaml2data, antenna_masks, enu_array, pol_order = read_yaml_antennas(
        station_dict
    )
    nant = 256
    assert len(antidx_yaml2data) == nant
    assert len(numpy.unique(antidx_yaml2data)) == nant
    assert len(antenna_masks) == nant
    assert sum(antenna_masks) == 16
    assert len(enu_array) == 3
    assert len(enu_array[0]) == nant
    assert abs(numpy.std(numpy.array(enu_array)) - 7.790431141833182) < 1e-7
    assert len(pol_order) == 4
    order = [0, 1, 2, 3]
    for i in range(4):
        assert pol_order[i] == order[i]


def test_read_yaml_config() -> None:
    """Test read of relevant parts of a station config file."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    location, antenna_masks, baselines, enu, pol, rotation = read_yaml_config(
        f"{cwd}/../testing_data/aavs3.yaml"
    )

    nant = 256
    nbaselines = nant * (nant + 1) // 2
    answer = (-2559377.99363595, 5095349.74793982, -2849124.06772244)
    assert abs(rotation - 0.0) < 1e-8
    for coord in range(3):
        assert abs(location.value[coord] - answer[coord]) < 1e-8
    assert len(antenna_masks) == nant
    assert sum(antenna_masks) == 16
    assert len(baselines) == 2
    assert len(baselines[0]) == nbaselines
    assert baselines[0].dtype == numpy.uint32
    assert (numpy.sort(baselines[0]) == numpy.sort(baselines[1])).all
    assert numpy.min(baselines[0]) == 0
    assert numpy.max(baselines[0]) == nant - 1
    assert len(enu) == 3
    assert len(enu[0]) == nant
    assert len(pol) == 4
    assert len(numpy.unique(pol)) == 4


def test_enu_to_geocentric() -> None:
    """Test antenna position coordinate change."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    location, _, _, enu_list, _, _ = read_yaml_config(
        f"{cwd}/../testing_data/aavs3.yaml"
    )
    enu = numpy.array(enu_list).T
    xyz = enu_to_geocentric(enu, location=location)

    assert enu.shape == xyz.shape
    answer = (-2559376.9688865, 5095350.38128287, -2849123.88149101)
    for coord in range(3):
        assert abs(xyz[0, coord] - answer[coord]) < 1e-8
    assert (
        numpy.max(
            numpy.abs(
                numpy.sum((enu - enu[[0], :]) ** 2, axis=1)
                - numpy.sum((xyz - xyz[[0], :]) ** 2, axis=1)
            )
        )
        < 1e-7
    )


def test_sdp_visibility_datamodel() -> None:
    """Test visibility xarray creation with a single time step."""
    nant = 4
    nbaselines = nant * (nant + 1) // 2
    nstokes = 4
    vis_data = numpy.random.randn(nbaselines, nstokes) + 0j
    vis_flags = numpy.zeros((nbaselines, nstokes), bool)
    enu = 35.0 * numpy.random.rand(nant, 3)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    ant1 = row[uppert]
    ant2 = col[uppert]
    uvw = enu[ant1] - enu[ant2]

    assert len(uvw) == nbaselines

    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")

    vis = sdp_visibility_datamodel(
        vis=vis_data,
        flags=vis_flags,
        uvw=uvw,
        ant1=ant1,
        ant2=ant2,
        location=read_yaml_location(station_dict),
        antpos_enu=enu,
        time=Time("2024-01-19T03:35:52.067"),
        int_time=1,
        frequency_mhz=100,
    )

    assert (vis_data[None, :, None, :] == vis.vis.data).all
    assert (vis_flags[None, :, None, :] == vis.flags.data).all
    assert (uvw[None, :, None, :] == vis.uvw.data).all
    assert (ant1 == vis.antenna1.data).all
    assert (ant2 == vis.antenna2.data).all


# pylint: disable=too-many-locals
def test_sdp_visibility_datamodel_multi() -> None:
    """Test visibility xarray creation with multiple time steps."""
    ntimes = 10
    nant = 4
    nbaselines = nant * (nant + 1) // 2
    nstokes = 4
    vis_data = numpy.random.randn(ntimes, nbaselines, nstokes) + 0j
    vis_flags = numpy.zeros((ntimes, nbaselines, nstokes), bool)
    enu = 35.0 * numpy.random.rand(nant, 3)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    ant1 = row[uppert]
    ant2 = col[uppert]
    uvw = numpy.tile(enu[ant1] - enu[ant2], (ntimes, 1, 1))

    assert len(ant1) == nbaselines
    assert len(ant1) == nbaselines
    assert len(uvw) == nbaselines

    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    location = read_yaml_location(station_dict)
    timestrings = []
    for itime in range(ntimes):
        timestrings.append(f"2024-01-19T03:35:5{itime}.067")
    time = Time(timestrings)

    vis = sdp_visibility_datamodel(
        vis=vis_data,
        flags=vis_flags,
        uvw=uvw,
        ant1=ant1,
        ant2=ant2,
        location=location,
        antpos_enu=enu,
        time=time,
        int_time=1,
        frequency_mhz=100,
    )

    assert (vis_data[:, :, None, :] == vis.vis.data).all
    assert (vis_flags[:, :, None, :] == vis.flags.data).all
    assert (uvw[:, :, None, :] == vis.uvw.data).all
    assert (ant1 == vis.antenna1.data).all
    assert (ant2 == vis.antenna2.data).all
