#!/usr/bin/env python

"""Tests for `ska_low_mccs_calibration.sky_model`."""

import numpy
import pytest
from astropy.coordinates import EarthLocation
from astropy.time import Time

from ska_low_mccs_calibration.sky_model import (
    dft_sky_to_vis,
    generate_ft_phasor,
    gsmap_lsm,
    gsmodel,
    solar_lsm,
)

SKY_MAX = pytest.approx(6249.417)


def test_gsmodel() -> None:
    """Test creation of pygdsm sky model."""
    nside = 4
    gsmap = gsmodel(100.0, nside, include_cmb=True)[0]
    assert len(gsmap) == 12 * nside * nside
    assert numpy.max(gsmap) == SKY_MAX


def test_gsmap_lsm() -> None:
    """Test down selection of pygdsm sky model."""
    nside = 4
    gsmap = gsmodel(100.0, nside, include_cmb=True)[0]
    time = Time("2024-01-19T03:35:52.067")
    location = EarthLocation.from_geodetic(
        116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
    )
    [azimuth_deg, elevation_deg, temp_lsm] = gsmap_lsm(
        gsmap, location=location, time=time
    )
    assert min(elevation_deg) >= 0
    argmax = numpy.argmax(temp_lsm)
    assert temp_lsm[argmax] == SKY_MAX
    assert abs(azimuth_deg[argmax] - 294.12819802606913) < 2e-7
    assert abs(elevation_deg[argmax] - 73.6902687579265) < 2e-7


def test_solar_lsm() -> None:
    """Test solar sky model."""
    time = Time("2024-01-19T03:35:52.067")
    location = EarthLocation.from_geodetic(
        116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
    )
    [azimuth_deg, elevation_deg, temp_sun] = solar_lsm(
        100.0, location=location, time=time
    )
    assert abs(temp_sun - 60.859707036830294) < 2e-7
    assert abs(azimuth_deg - 62.846257623189935) < 2e-7
    assert abs(elevation_deg - 77.3784655038844) < 2e-7


def test_generate_ft_phasor() -> None:
    """Test DFT phase generation."""
    azimuth_deg, elevation_deg, _ = solar_lsm(
        100.0,
        location=EarthLocation.from_geodetic(
            116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
        ),
        time=Time("2024-01-19T03:35:52.067"),
    )
    # generate some random uv locations in nsec
    numpy.random.seed(seed=10000000)
    delay_ns = 35 * numpy.random.rand(3, 4) / 3e8 * 1e9
    frequency_mhz = 100.0
    ft_arg_complex = generate_ft_phasor(
        delay_ns, frequency_mhz, [azimuth_deg], [elevation_deg]
    )

    azimuth = azimuth_deg * numpy.pi / 180.0
    elevation = elevation_deg * numpy.pi / 180.0
    l_pix = numpy.cos(elevation) * numpy.sin(azimuth)
    m_pix = numpy.cos(elevation) * numpy.cos(azimuth)
    n_pix = numpy.sin(elevation)
    u = delay_ns[0] * 1e-09 * frequency_mhz * 1e6
    v = delay_ns[1] * 1e-09 * frequency_mhz * 1e6
    w = delay_ns[2] * 1e-09 * frequency_mhz * 1e6
    answer = numpy.exp(-2j * numpy.pi * (u * l_pix + v * m_pix + w * n_pix))
    assert numpy.allclose(answer, ft_arg_complex.T, atol=2e-7)


def test_dft_sky_to_vis() -> None:
    """Test DFT calculation."""
    [azimuth_deg, elevation_deg, temp_sun] = solar_lsm(
        100.0,
        location=EarthLocation.from_geodetic(
            116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
        ),
        time=Time("2024-01-19T03:35:52.067"),
    )
    # generate some random uv locations in nsec
    numpy.random.seed(seed=10000000)
    delay_ns = 35 * numpy.random.rand(3, 6) / 3e8 * 1e9
    frequency_mhz = 100.0
    ft_arg_complex = generate_ft_phasor(
        delay_ns, frequency_mhz, [azimuth_deg], [elevation_deg]
    )
    # 4 pol x 3 antenna x 1 pix
    eep_array = numpy.random.rand(4, 3, 1) + 1j * numpy.random.rand(4, 3, 1)
    eep_list = [eep_array[0], eep_array[1], eep_array[2], eep_array[3]]
    ant1 = numpy.array([0, 0, 0, 1, 1, 2])
    ant2 = numpy.array([0, 1, 2, 1, 2, 2])
    index_array = numpy.array([ant1, ant2]).T

    model_products = dft_sky_to_vis(eep_list, index_array, ft_arg_complex, [temp_sun])

    answer = (
        (
            eep_array[0, ant1, 0] * eep_array[0, ant2, 0].conj()
            + eep_array[1, ant1, 0] * eep_array[1, ant2, 0].conj()
        )
        * ft_arg_complex[:, 0]
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[0], atol=2e-7)

    answer = (
        (
            eep_array[0, ant1, 0] * eep_array[2, ant2, 0].conj()
            + eep_array[1, ant1, 0] * eep_array[3, ant2, 0].conj()
        )
        * ft_arg_complex[:, 0]
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[1], atol=2e-7)

    answer = (
        (
            eep_array[2, ant1, 0] * eep_array[0, ant2, 0].conj()
            + eep_array[3, ant1, 0] * eep_array[1, ant2, 0].conj()
        )
        * ft_arg_complex[:, 0]
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[2], atol=2e-7)

    answer = (
        (
            eep_array[2, ant1, 0] * eep_array[2, ant2, 0].conj()
            + eep_array[3, ant1, 0] * eep_array[3, ant2, 0].conj()
        )
        * ft_arg_complex[:, 0]
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[3], atol=2e-7)


def test_dft_sky_to_vis_ave_eep() -> None:
    """Test DFT calculation."""
    [azimuth_deg, elevation_deg, temp_sun] = solar_lsm(
        100.0,
        location=EarthLocation.from_geodetic(
            116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
        ),
        time=Time("2024-01-19T03:35:52.067"),
    )
    # generate some random uv locations in nsec
    numpy.random.seed(seed=10000000)
    delay_ns = 35 * numpy.random.rand(3, 6) / 3e8 * 1e9
    frequency_mhz = 100.0
    ft_arg_complex = generate_ft_phasor(
        delay_ns, frequency_mhz, [azimuth_deg], [elevation_deg]
    )
    # 4 pol x 1 average x 1 pix
    eeparr = numpy.random.rand(4, 1, 1) + 1j * numpy.random.rand(4, 1, 1)
    eep_list = [eeparr[0], eeparr[1], eeparr[2], eeparr[3]]
    ant1 = numpy.array([0, 0, 0, 1, 1, 2])
    ant2 = numpy.array([0, 1, 2, 1, 2, 2])
    index_array = numpy.array([ant1, ant2]).T

    model_products = dft_sky_to_vis(eep_list, index_array, ft_arg_complex, [temp_sun])

    answer = (
        ft_arg_complex[:, 0]
        * (eeparr[0, 0] * eeparr[0, 0].conj() + eeparr[1, 0] * eeparr[1, 0].conj())
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[0], atol=2e-7)

    answer = (
        ft_arg_complex[:, 0]
        * (eeparr[0, 0] * eeparr[2, 0].conj() + eeparr[1, 0] * eeparr[3, 0].conj())
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[1], atol=2e-7)

    answer = (
        ft_arg_complex[:, 0]
        * (eeparr[2, 0] * eeparr[0, 0].conj() + eeparr[3, 0] * eeparr[1, 0].conj())
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[2], atol=2e-7)

    answer = (
        ft_arg_complex[:, 0]
        * (eeparr[2, 0] * eeparr[2, 0].conj() + eeparr[3, 0] * eeparr[3, 0].conj())
        * temp_sun
    )
    assert numpy.allclose(answer, model_products[3], atol=2e-7)


def test_dft_sky_to_vis_no_eeps() -> None:
    """Test DFT calculation without EEPs."""
    [azimuth_deg, elevation_deg, temp_sun] = solar_lsm(
        100.0,
        location=EarthLocation.from_geodetic(
            116.670233065, -26.704078296, height=359.495, ellipsoid="WGS84"
        ),
        time=Time("2024-01-19T03:35:52.067"),
    )
    # generate some random uv locations in nsec
    numpy.random.seed(seed=10000000)
    delay_ns = 35 * numpy.random.rand(3, 6) / 3e8 * 1e9
    frequency_mhz = 100.0
    ft_arg_complex = generate_ft_phasor(
        delay_ns, frequency_mhz, [azimuth_deg], [elevation_deg]
    )
    ant1 = numpy.array([0, 0, 0, 1, 1, 2])
    ant2 = numpy.array([0, 1, 2, 1, 2, 2])
    index_array = numpy.array([ant1, ant2]).T

    model_products = dft_sky_to_vis(None, index_array, ft_arg_complex, [temp_sun])

    answer = ft_arg_complex[:, 0] * temp_sun
    assert numpy.allclose(model_products[0], answer, atol=2e-7)
    assert numpy.allclose(model_products[1], 0, atol=2e-7)
    assert numpy.allclose(model_products[2], 0, atol=2e-7)
    assert numpy.allclose(model_products[3], answer, atol=2e-7)
