"""Tests for `ska_low_mccs_calibration.qa`."""

import matplotlib
import numpy
import pandas
import pytest
from astropy import units
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.science_data_model import PolarisationFrame, ReceptorFrame
from ska_sdp_datamodels.visibility import Visibility

from ska_low_mccs_calibration.calibration import calibrate_mccs_visibility
from ska_low_mccs_calibration.qa import (
    generate_calibration_info,
    generate_closure_comparison_plots,
    generate_gaintable_plots,
    generate_gaintable_scatter_plots,
    generate_visibility_comparison_plots,
    generate_visibility_plots,
)
from ska_low_mccs_calibration.utils import enu_to_geocentric


class TestQualityAssurance:
    """Class to hold tests of QA routines."""

    @pytest.fixture(autouse=True)
    def create_datasets(self) -> None:  # pylint: disable=too-many-locals
        """Fixture to generate visibilities."""
        frequency = numpy.arange(150e6, 250e6, 781.25e3)
        channel_bandwidth = 781.25e3 * numpy.ones(frequency.shape)
        time = numpy.array([Time("2024-01-01T00:00:00.0").mjd * 24 * 3600])
        integration_time = numpy.array([1.0])
        nant = 6
        polarisation_frame = PolarisationFrame("linear")
        npol = len(polarisation_frame.translations)
        antid = numpy.arange(nant)
        ones = numpy.ones(nant, "int")
        mask = antid[numpy.newaxis, :] - antid[:, numpy.newaxis] >= 0
        ant1 = (ones[numpy.newaxis, :] * antid[:, numpy.newaxis])[mask]
        ant2 = (antid[numpy.newaxis, :] * ones[:, numpy.newaxis])[mask]
        # pylint: disable-next=attribute-defined-outside-init
        self.enu = 1000 * numpy.random.normal(0, 1e3, (nant, 3))
        assert len(time) == 1
        uvw = numpy.zeros((1, len(ant1), 3))
        for dim in range(3):
            uvw[0, :, dim] = self.enu[ant1, dim] - self.enu[ant2, dim]
        vis = numpy.ones((1, len(ant1), len(frequency), npol), "complex")
        flags = numpy.zeros(shape=vis.shape)
        source = "anonymous"
        location = EarthLocation.from_geodetic(
            116.67, -26.7, height=359.5, ellipsoid="WGS84"
        )
        # pylint: disable-next=attribute-defined-outside-init
        self.vis = Visibility.constructor(
            frequency=frequency,
            channel_bandwidth=channel_bandwidth,
            phasecentre=SkyCoord(
                0.0 * units.deg, 0.0 * units.deg  # pylint: disable=no-member
            ),
            configuration=Configuration().constructor(
                name="test_array",
                names=[f"ant{x:02d}" for x in range(1, nant + 1)],
                stations=[f"{x}" for x in range(1, nant + 1)],
                location=location,
                xyz=enu_to_geocentric(self.enu, location=location),
                receptor_frame=ReceptorFrame("linear"),
                diameter=1.0,  # Errors if not set
                mount="altaz",
            ),
            uvw=uvw,
            time=time,
            vis=vis,
            weight=None,
            integration_time=integration_time,
            flags=flags,
            baselines=pandas.MultiIndex.from_tuples(
                [(ant1[bl], ant2[bl]) for bl in range(vis.shape[1])],
                names=("antenna1", "antenna2"),
            ),
            polarisation_frame=polarisation_frame,
            source=source,
            meta=None,
            low_precision="float64",
        )
        # pylint: disable-next=attribute-defined-outside-init
        self.model = self.vis.copy()
        # pylint: disable-next=attribute-defined-outside-init
        self.gaintable = create_gaintable_from_visibility(self.vis, jones_type="G")

    def test_generate_calibration_info(self) -> None:
        """Test calibration output QA info."""
        time = Time("2024-01-01T00:00:00.0")
        location = EarthLocation.from_geodetic(
            116.67, -26.7, height=359.5, ellipsoid="WGS84"
        )
        calibration_info = generate_calibration_info(
            vis=self.vis,
            modelvis=self.model,
            masked_antennas_init=numpy.array([1]),
            masked_antennas_final=numpy.array([1, 3]),
            location=location,
            time=time,
            xy_phase=10 * numpy.pi / 180,
            sun_adjustment_factor=1.5,
        )
        print(calibration_info)
        assert len(getattr(calibration_info, "corrcoeff")) == 4
        assert len(getattr(calibration_info, "residual_std")) == 4
        assert len(getattr(calibration_info, "residual_max")) == 4
        assert getattr(calibration_info, "xy_phase") == 10
        assert getattr(calibration_info, "n_masked_initial") == 1
        assert getattr(calibration_info, "n_masked_final") == 2
        assert (
            numpy.abs(
                getattr(calibration_info, "lst")
                - time.sidereal_time("apparent", location).value
            )
            < 1e-8
        )
        assert (
            numpy.abs(
                getattr(calibration_info, "galactic_centre_elevation")
                - 46.08969765433979
            )
            < 1e-8
        )
        assert (
            numpy.abs(getattr(calibration_info, "sun_elevation") - 32.34438301943652)
            < 1e-8
        )
        assert getattr(calibration_info, "sun_adjustment_factor") == 1.5

    def test_generate_calibration_info_output(self) -> None:
        """Test calibration output QA info."""
        _, _, _, _, calibration_info = calibrate_mccs_visibility(
            self.vis,
            skymodel=self.model,
        )
        assert len(getattr(calibration_info, "corrcoeff")) == 4
        assert len(getattr(calibration_info, "residual_std")) == 4
        assert len(getattr(calibration_info, "residual_max")) == 4
        assert getattr(calibration_info, "xy_phase") == 0
        assert getattr(calibration_info, "n_masked_initial") == 0
        assert getattr(calibration_info, "n_masked_final") == 0
        assert numpy.abs(getattr(calibration_info, "lst") - 14.454752434199758) < 1e-8
        assert (
            numpy.abs(
                getattr(calibration_info, "galactic_centre_elevation")
                - 46.08969765433979
            )
            < 1e-8
        )
        assert (
            numpy.abs(getattr(calibration_info, "sun_elevation") - 32.34438301943652)
            < 1e-8
        )
        assert getattr(calibration_info, "sun_adjustment_factor") == 1.0

    def test_generate_gaintable_plots(self) -> None:
        """Test standard gaintable plots."""
        fig = generate_gaintable_plots(self.gaintable, masked_antennas=numpy.array([]))
        assert isinstance(fig, matplotlib.figure.Figure)
        assert len(fig.axes) == 4

    def test_generate_gaintable_scatter_plots(self) -> None:
        """Test standard gaintable scatter plots."""
        fig = generate_gaintable_scatter_plots(
            self.gaintable, masked_antennas=numpy.array([]), enu=self.enu
        )
        assert isinstance(fig, matplotlib.figure.Figure)
        npol = 4
        npanel = 2 * npol
        ncolorbar = 2 * npol
        assert len(fig.axes) == npanel + ncolorbar

    def test_generate_visibility_plots(self) -> None:
        """Test standard visibility plots."""
        fig = generate_visibility_plots(
            self.vis, self.model, masked_antennas=numpy.array([])
        )
        assert isinstance(fig, matplotlib.figure.Figure)
        npol = 4
        npanel = 6 * npol
        assert len(fig.axes) == npanel

    def test_generate_visibility_comparison_plots(self) -> None:
        """Test visibility vs model comparison plots."""
        fig = generate_visibility_comparison_plots(
            self.vis, self.model, masked_antennas=numpy.array([])
        )
        assert isinstance(fig, matplotlib.figure.Figure)
        npol = 4
        npanel = 3 * npol
        assert len(fig.axes) == npanel

    def test_generate_closure_comparison_plots(self) -> None:
        """Test visibility closure vs model comparison plots."""
        npol = 2
        npanel = 3 * npol
        ncolorbar = 2

        # Minimal call
        fig = generate_closure_comparison_plots(
            self.vis, self.model, masked_antennas=numpy.array([])
        )
        assert isinstance(fig, matplotlib.figure.Figure)
        assert len(fig.axes) == npanel

        # Call with extra arguments
        fig = generate_closure_comparison_plots(
            self.vis,
            self.model,
            masked_antennas=numpy.array([]),
            min_uv=10,
            decimate=1,
            ctype="uvdiff",
        )
        assert isinstance(fig, matplotlib.figure.Figure)
        assert len(fig.axes) == npanel + ncolorbar
