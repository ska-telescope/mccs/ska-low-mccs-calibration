#!/usr/bin/env python
# need to learn about typing.Generator return type for create_eeps
# mypy: disable-error-code="no-untyped-def"

"""Tests for `ska_low_mccs_calibration.calibration`."""

import os
from typing import List, Union

import numpy
import numpy.typing as npt
import pytest
import xarray
from astropy import constants as const
from astropy import units
from astropy.time import Time
from numpy import random

from ska_low_mccs_calibration.calibration import (
    adjust_gsm,
    calibrate_mccs_visibility,
    xy_phase_estimate,
)
from ska_low_mccs_calibration.sky_model import predict_vis, solar_lsm
from ska_low_mccs_calibration.utils import (
    read_yaml_location,
    read_yaml_station,
    sdp_visibility_datamodel,
)


def test_adjust_gsm() -> None:
    """Test visibility xarray creation with a single time step."""
    nant = 6
    nbaselines = nant * (nant + 1) // 2
    nstokes = 4
    numpy.random.seed(seed=10000000)
    enu = 35.0 * numpy.random.rand(nant, 3)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    vis = sdp_visibility_datamodel(
        vis=numpy.zeros((nbaselines, nstokes), complex),
        flags=numpy.zeros((nbaselines, nstokes), bool),
        uvw=enu[row[uppert]] - enu[col[uppert]],
        ant1=row[uppert],
        ant2=col[uppert],
        location=read_yaml_location(station_dict),
        antpos_enu=enu,
        time=Time("2024-01-19T03:35:52.067"),
        int_time=1,
        frequency_mhz=100,
    )

    comp1 = numpy.random.randn(nbaselines, nstokes) + 1j * numpy.random.randn(
        nbaselines, nstokes
    )
    comp2 = numpy.random.randn(nbaselines, nstokes) + 1j * numpy.random.randn(
        nbaselines, nstokes
    )

    model = vis.copy(deep=True)
    model.vis.data[0, :, 0, :] = comp1 + comp2
    calib = vis.copy(deep=True)
    calib.vis.data[0, :, 0, :] = comp1 + 1.5 * comp2

    model, adjustment_factor = adjust_gsm(
        calib,
        model,
        comp1.T,
        comp2.T,
    )
    assert numpy.abs(adjustment_factor - 1.5) < 1e-7
    assert numpy.allclose(model.vis.data, calib.vis.data, atol=1e-7)


def test_xy_phase_estimate() -> None:
    """Test visibility xarray creation with a single time step."""
    nant = 6
    nbaselines = nant * (nant + 1) // 2
    nstokes = 4
    numpy.random.seed(seed=10000000)
    enu = 35.0 * numpy.random.rand(nant, 3)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    cwd = os.path.dirname(os.path.realpath(__file__))
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    vis = sdp_visibility_datamodel(
        vis=numpy.zeros((nbaselines, nstokes), complex),
        flags=numpy.zeros((nbaselines, nstokes), bool),
        uvw=enu[row[uppert]] - enu[col[uppert]],
        ant1=row[uppert],
        ant2=col[uppert],
        location=read_yaml_location(station_dict),
        antpos_enu=enu,
        time=Time("2024-01-19T03:35:52.067"),
        int_time=1,
        frequency_mhz=100,
    )

    vis.vis.data[0, :, 0, 1] = numpy.random.randn(nbaselines) + 1j * numpy.random.randn(
        nbaselines
    )
    vis.vis.data[0, :, 0, 2] = numpy.random.randn(nbaselines) + 1j * numpy.random.randn(
        nbaselines
    )
    model = vis.copy(deep=True)
    model.vis.data[0, :, 0, 1] *= numpy.exp(+1j * 1.6)
    model.vis.data[0, :, 0, 2] *= numpy.exp(-1j * 1.6)

    xy_phase = xy_phase_estimate(vis, model)

    assert numpy.abs(xy_phase - 1.6) < 1e-7


def save_eeps(
    eeps: list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]],
) -> List[str]:
    """Save EEPs in npy format."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta] = eeps

    sname_x_phi = f"{cwd}/../testing_data/single_120MHz_Xpol_phi.npy"
    sname_x_theta = f"{cwd}/../testing_data/single_120MHz_Xpol_theta.npy"
    sname_y_phi = f"{cwd}/../testing_data/single_120MHz_Ypol_phi.npy"
    sname_y_theta = f"{cwd}/../testing_data/single_120MHz_Ypol_theta.npy"
    numpy.save(sname_x_phi, eep_x_phi.conj().astype(numpy.complex64))
    numpy.save(sname_x_theta, eep_x_theta.conj().astype(numpy.complex64))
    numpy.save(sname_y_phi, eep_y_phi.conj().astype(numpy.complex64))
    numpy.save(sname_y_theta, eep_y_theta.conj().astype(numpy.complex64))

    dname_x_phi = f"{cwd}/../testing_data/double_120MHz_Xpol_phi.npy"
    dname_x_theta = f"{cwd}/../testing_data/double_120MHz_Xpol_theta.npy"
    dname_y_phi = f"{cwd}/../testing_data/double_120MHz_Ypol_phi.npy"
    dname_y_theta = f"{cwd}/../testing_data/double_120MHz_Ypol_theta.npy"
    numpy.save(dname_x_phi, eep_x_phi.conj().astype(numpy.complex128))
    numpy.save(dname_x_theta, eep_x_theta.conj().astype(numpy.complex128))
    numpy.save(dname_y_phi, eep_y_phi.conj().astype(numpy.complex128))
    numpy.save(dname_y_theta, eep_y_theta.conj().astype(numpy.complex128))

    return [
        sname_x_phi,
        sname_x_theta,
        sname_y_phi,
        sname_y_theta,
        dname_x_phi,
        dname_x_theta,
        dname_y_phi,
        dname_y_theta,
    ]


def remove_eeps(names: list[str]) -> None:
    """Remove EEPs generated by save_eeps."""
    for fname in names:
        os.remove(fname)


def generate_bl_ids(nant: int) -> npt.NDArray[numpy.int64]:
    """Generate baseline IDs."""
    numpy.random.seed(seed=10000000)
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    return numpy.array([row[uppert], col[uppert]], numpy.int64)


def generate_empty_vis(
    nant: int,
    enu: npt.NDArray[numpy.float64],
    bl_ids: npt.NDArray[numpy.int64],
    location: units.Quantity,
    time: units.Quantity,
) -> xarray.Dataset:
    """Generate visibility dataset initialised to zero."""
    nbaselines = nant * (nant + 1) // 2
    nstokes = 4
    ant1 = bl_ids[0]
    ant2 = bl_ids[1]
    uvw = enu[ant1] - enu[ant2]
    return sdp_visibility_datamodel(
        vis=numpy.zeros((nbaselines, nstokes), complex),
        flags=numpy.zeros((nbaselines, nstokes), bool),
        uvw=uvw,
        ant1=ant1,
        ant2=ant2,
        location=location,
        antpos_enu=enu,
        time=time,
        int_time=1,
        frequency_mhz=120,
    )


def generate_model_vis(
    eeps: Union[None, list[npt.NDArray[Union[numpy.complex64, numpy.complex128]]]],
    nant: int,
) -> xarray.Dataset:
    """Generate model visibilities."""
    cwd = os.path.dirname(os.path.realpath(__file__))
    numpy.random.seed(seed=10000000)
    enu = 35.0 * numpy.random.rand(nant, 3)
    bl_ids = generate_bl_ids(nant)
    station_dict = read_yaml_station(f"{cwd}/../testing_data/aavs3.yaml")
    location = read_yaml_location(station_dict)
    time = Time("2024-01-19T03:35:52.067")
    model = generate_empty_vis(nant, enu, bl_ids, location, time)

    # Generate local sky model for the sun
    [az, el, T] = solar_lsm(120, location=location, time=time)
    assert el > 0
    delay = (enu[bl_ids[0]] - enu[bl_ids[1]]).T
    # Convert delays from m to ns
    delay *= 1e9 / const.c.value  # pylint: disable=no-member
    model.vis.data = numpy.array(
        predict_vis(eeps, 0, bl_ids.T, delay, 120, [az], [el], [T])
    ).T.reshape(model.vis.shape)

    if eeps is not None:
        model01 = numpy.array(
            [
                -33.41976608 - 4.68788124e01j,
                2.19403979 + 4.60120737e-01j,
                2.01441975 + 4.21622152e-01j,
                -44.65912895 - 3.96643185e01j,
            ]
        )
    else:
        model01 = numpy.array(
            [
                -37.10683348 - 48.12689237j,
                0.0 + 0.0j,
                0.0 + 0.0j,
                -37.10683348 - 48.12689237j,
            ]
        )
    assert numpy.allclose(model.vis.data[0, 1, 0, :], model01, atol=1e-7)

    return model


def generate_vis(model: xarray.Dataset, nant: int) -> xarray.Dataset:
    """Generate true visibilities."""
    # Add corruptions to model vis to form the "true" observed set
    nbaselines = nant * (nant + 1) // 2
    row = numpy.tile(numpy.arange(nant), (nant, 1)).T
    col = numpy.tile(numpy.arange(nant), (nant, 1))
    uppert = row <= col
    ant1 = row[uppert]
    ant2 = col[uppert]
    numpy.random.seed(seed=20000000)
    jones = numpy.zeros((nant, 2, 2), complex)
    jones[:] = numpy.eye(2)
    jones += random.normal(0, 0.1, jones.shape)
    vis = model.copy(deep=True)
    vis.vis.data = numpy.einsum(  # pylint: disable=too-many-function-args
        "bxp,tbfpq,byq->tbfxy",
        jones[ant1],
        model.vis.data.reshape(1, nbaselines, 1, 2, 2),
        jones[ant2].conj(),
    ).reshape(1, nbaselines, 1, 4)

    vis01 = (
        jones[ant1[1]]
        @ model.vis.data[0, 1, 0, :].reshape(2, 2)
        @ jones[ant2[1]].conj().T
    )
    assert numpy.allclose(vis.vis.data[0, 1, 0, :].reshape(2, 2), vis01, atol=1e-12)

    return vis


class TestCalibration:
    """Class to hold tests of EEPs."""

    @pytest.fixture(autouse=True)
    def create_eeps(self):
        """Fixture to setup and teardown test EEP data."""
        # EEPs are complex with regular sampling (resolution res) in az,za
        # for each antenna
        self.nantenna = 6  # pylint: disable=attribute-defined-outside-init
        self.res = 15  # pylint: disable=attribute-defined-outside-init
        azimuth = numpy.linspace(0.0, 360.0, num=int(360 / self.res) + 1, endpoint=True)
        z_angle = numpy.linspace(0.0, 90.0, num=int(90 / self.res) + 1, endpoint=True)
        azimuth *= numpy.pi / 180.0
        z_angle *= numpy.pi / 180.0
        eep_shape = (len(azimuth), len(z_angle), self.nantenna)

        # convert azimuth from the EEP N-from-E def to a standard E-from-N bearing.
        bearing = numpy.pi / 2 - azimuth

        az, za = numpy.meshgrid(
            bearing,
            z_angle,
            indexing="ij",
        )

        numpy.random.seed(seed=10000000)
        eep_x_phi = (-numpy.cos(az))[:, :, None] * numpy.exp(
            1j * random.normal(0, 0.1, eep_shape)
        )
        eep_x_theta = (-numpy.cos(za) * numpy.sin(az))[:, :, None] * numpy.exp(
            1j * random.normal(0, 0.1, eep_shape)
        )
        eep_y_phi = (numpy.sin(az))[:, :, None] * numpy.exp(
            1j * random.normal(0, 0.1, eep_shape)
        )
        eep_y_theta = (-numpy.cos(za) * numpy.cos(az))[:, :, None] * numpy.exp(
            1j * random.normal(0, 0.1, eep_shape)
        )
        assert eep_x_phi.dtype == numpy.complex128

        # save EEPs for auto generation of model vis in calibrate_mccs_visibility
        # pylint: disable-next=attribute-defined-outside-init
        self.eeps = [eep_x_phi, eep_x_theta, eep_y_phi, eep_y_theta]
        names = save_eeps(self.eeps)

        yield

        remove_eeps(names)

    @pytest.fixture(autouse=True)
    def create_vis(self) -> None:
        """Fixture to generate visibilities."""
        # pylint: disable-next=attribute-defined-outside-init
        self.model = generate_model_vis(self.eeps, self.nantenna)
        # pylint: disable-next=attribute-defined-outside-init
        self.vis = generate_vis(self.model, self.nantenna)

    def test_calibrate_mccs_visibility_user_model(self) -> None:
        """Test calibration with user-defined model visibilities."""
        _, _, calib, _, _ = calibrate_mccs_visibility(
            self.vis,
            skymodel=self.model,
            masked_antennas=numpy.array([]),
            jones_solve=True,
        )
        assert numpy.allclose(calib.vis.data, self.model.vis.data, atol=1e-7)

    def test_calibrate_mccs_visibility_auto_model(self) -> None:
        """Test calibration with auto-generated model vis and single precision."""
        cwd = os.path.dirname(os.path.realpath(__file__))

        # save EEPs for use in auto model generation
        _, _, calib, _, _ = calibrate_mccs_visibility(
            self.vis,
            skymodel="sun",
            masked_antennas=numpy.array([]),
            jones_solve=True,
            eep_path=f"{cwd}/../testing_data",
            eep_filebase="single_",
            eep_rotation_deg=0,
            dtype=numpy.complex64,
        )
        assert numpy.allclose(calib.vis.data, self.model.vis.data, atol=1e-7)

        # save EEPs for use in auto model generation
        _, _, calib, _, _ = calibrate_mccs_visibility(
            self.vis,
            skymodel="sun",
            masked_antennas=numpy.array([]),
            jones_solve=True,
            eep_path=f"{cwd}/../testing_data",
            eep_filebase="double_",
            eep_rotation_deg=0,
            dtype=numpy.complex128,
        )
        assert numpy.allclose(calib.vis.data, self.model.vis.data, atol=1e-7)

    def test_calibrate_mccs_visibility_ignore_eeps(self) -> None:
        """Test calibration without EEPs."""
        model = generate_model_vis(None, self.nantenna)
        vis = model.copy(deep=True)
        reference_ant = 0
        delayed_ant = 1
        phi = 0.3
        vis.vis.data[0, vis.antenna1.data == delayed_ant, 0, 0] *= numpy.exp(1j * phi)
        vis.vis.data[0, vis.antenna2.data == delayed_ant, 0, 0] *= numpy.exp(-1j * phi)

        # save EEPs for use in auto model generation
        gaintable, model_out, calib, _, _ = calibrate_mccs_visibility(
            vis,
            skymodel="sun",
            masked_antennas=numpy.array([]),
            jones_solve=False,
            ignore_eeps=True,
        )
        assert (
            numpy.max(
                numpy.abs(
                    numpy.angle(gaintable.gain.data[0, delayed_ant, 0, 0, 0])
                    - numpy.angle(gaintable.gain.data[0, reference_ant, 0, 0, 0])
                    - phi
                )
            )
            < 1e-6
        )
        assert (
            numpy.max(
                numpy.abs(
                    numpy.angle(gaintable.gain.data[0, delayed_ant, 0, 1, 1])
                    - numpy.angle(gaintable.gain.data[0, reference_ant, 0, 1, 1])
                )
            )
            < 1e-7
        )
        assert numpy.angle(gaintable.gain.data[0, delayed_ant, 0, 0, 1]) == 0
        assert numpy.angle(gaintable.gain.data[0, delayed_ant, 0, 1, 0]) == 0
        assert numpy.allclose(model_out.vis.data, model.vis.data, atol=1e-7)
        assert numpy.allclose(calib.vis.data, model.vis.data, atol=1e-4)
