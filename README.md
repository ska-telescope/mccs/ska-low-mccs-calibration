# ska-low-mccs-calibration

This project defines the station calibration subsystem for the SKA-Low telescope. It is part of the MCCS subsystem.


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-mccs-calibration/badge/?version=latest)](https://developer.skao.int/projects/ska-low-mccs-calibration/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-mccs-calibration documentation](https://developer.skatelescope.org/projects/ska-low-mccs-calibration/en/latest/index.html "SKA Developer Portal: ska-low-mccs-calibration documentation")

## Features

* TODO
